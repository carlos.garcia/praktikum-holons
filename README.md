# Holeg Simulator

Cyber-physical systems simulation software following a Holon-based smart grid system.

## Installation
This project uses gradle as build tool. Gradle is not needed to be installed on your local machine.
#### Commandline
When using the commandline 'gradlew' is used. For Windows change with 'gradlew.bat'.
```
./gradlew build
./gradlew run
```
#### Eclipse
When using the Eclipse IDE to develop. Import the project as existing gradle project.
![alt text](res/images/git/import.png "Import")
![alt text](res/images/git/gradle.png "Gradle")

## License

This project is licensed under a modified MIT License, see
the [LICENSE.md](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/src/master/license.md)
file for details.

## Built With
* [Gradle](https://gradle.org/) - Build tool
* [Java 17](https://openjdk.java.net/projects/jdk/17/) - Java SE version
* [JUnit](http://junit.org) - Test system
* [Gson](https://github.com/google/gson) - Used for Saving/Loading
* [MigLayout](https://github.com/mikaelgrev/miglayout) - Extends Swing
* [XChart](https://github.com/knowm/XChart) - For data visualization

## Code Style
This project uses [Google Code Style](https://google.github.io/styleguide/javaguide.html) and corresponding [Code Schema](https://github.com/google/styleguide).
## Contributors

* Rolf Egert
* Tom Troppmann
### Past Contributors

* Andreas Tundis
* Carlos Garcia
* Andreas T. Meyer-Berg
* Ludwig Tietze
* Antonio Schneider
* Isabella Dix
* Kevin Julian Trometer
* Dominik Fabio Rieder
* Teh-Hai Julian Zheng
* Edgardo Ernesto Palza Paredes
* Jessey Steven Widhalm



## Under contract from:

* Darmstadt University of Technology
* Department of Computer Science, Telecooperation
* Hochschulstr. 10
* Informatik Telecooperation Lab (TK)
* D-64289 Darmstadt, Germany
