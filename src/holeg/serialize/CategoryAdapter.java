package holeg.serialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import holeg.model.AbstractCanvasObject;
import holeg.model.HolonObject;
import holeg.model.HolonSwitch;
import holeg.ui.view.category.Category;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CategoryAdapter implements JsonSerializer<Category>, JsonDeserializer<Category> {

  public static final Gson Gson = initGson();
  public static final Type CategorySet = new TypeToken<Set<Category>>() {
  }.getType();
  private static final Type HolonObjectList = new TypeToken<List<HolonObject>>() {
  }.getType();
  private static final Type HolonSwitchList = new TypeToken<List<HolonSwitch>>() {
  }.getType();


  private static Gson initGson() {
    GsonBuilder gson = new GsonBuilder();
    gson.registerTypeAdapter(Category.class, new CategoryAdapter());
    gson.registerTypeAdapterFactory(new PostDeserializeEnabler());
    gson.serializeNulls();
    return gson.create();
  }

  @Override
  public JsonElement serialize(Category src, Type typeOfSrc, JsonSerializationContext context) {
    JsonObject object = new JsonObject();
    object.addProperty("name", src.getName());

    Map<Class<?>, List<AbstractCanvasObject>> map =
        src.getObjects().stream()
            .collect(Collectors.groupingBy(Object::getClass));
    object.add("objects", context.serialize(map.get(HolonObject.class), HolonObjectList));
    object.add("switches", context.serialize(map.get(HolonSwitch.class), HolonSwitchList));
    return object;
  }

  @Override
  public Category deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    JsonObject object = json.getAsJsonObject();
    Category cat = new Category(object.get("name").getAsString());
    if (object.has("objects") && !object.get("objects").isJsonNull()) {
      cat.getObjects()
          .addAll(context.deserialize(object.getAsJsonArray("objects"), HolonObjectList));
    }
    if (object.has("switches") && !object.get("switches").isJsonNull()) {
      cat.getObjects()
          .addAll(context.deserialize(object.getAsJsonArray("switches"), HolonSwitchList));
    }
    return cat;
  }
}
