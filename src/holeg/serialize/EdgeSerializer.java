package holeg.serialize;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import holeg.model.Edge;
import java.lang.reflect.Type;

public class EdgeSerializer implements JsonSerializer<Edge> {

  @Override
  public JsonElement serialize(Edge edge, Type typeOfSrc, JsonSerializationContext context) {
    JsonObject jsonObj = new JsonObject();
    jsonObj.addProperty("idA", edge.getA().getId());
    jsonObj.addProperty("idB", edge.getB().getId());
    jsonObj.addProperty("maxCapacity", edge.maxCapacity);
    jsonObj.addProperty("mode", edge.mode.toString());
    return jsonObj;
  }
}
