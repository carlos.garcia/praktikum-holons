package holeg.serialize;

public interface PostDeserialize {

  void postDeserialize();
}
