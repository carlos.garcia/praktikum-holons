package holeg.serialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import holeg.model.AbstractCanvasObject;
import holeg.model.Edge;
import holeg.model.GroupNode;
import holeg.model.Model;
import holeg.ui.model.IdCounter;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ModelDeserializer implements JsonDeserializer<Model> {

  public static final Gson gson = initGson();
  private static final Logger log = Logger.getLogger(ModelDeserializer.class.getName());
  private final static Type edgeSetType = TypeToken.getParameterized(HashSet.class, Edge.class)
      .getType();
  private final EdgeDeserializer edgeDeserializer = new EdgeDeserializer();
  private final Gson edgeGson = new GsonBuilder().registerTypeAdapter(Edge.class, edgeDeserializer)
      .create();

  private static Gson initGson() {
    GsonBuilder builder = new GsonBuilder();
    builder.registerTypeAdapter(Edge.class, new EdgeSerializer());
    builder.registerTypeAdapter(Model.class, new ModelDeserializer());
    builder.registerTypeAdapterFactory(new PostDeserializeEnabler());

    builder.serializeNulls();
    builder.setPrettyPrinting();
    return builder.create();
  }

  @Override
  public Model deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    JsonObject jsonObj = json.getAsJsonObject();

    Model model = new Model();
    GroupNode canvas = context.deserialize(jsonObj.getAsJsonObject("canvas"), GroupNode.class);
    model.setCanvas(canvas);
    edgeDeserializer.idMap = canvas.getAllObjectsRecursive()
        .collect(Collectors.toMap(AbstractCanvasObject::getId, Function.identity()));
    model.setEdgesOnCanvas(edgeGson.fromJson(jsonObj.getAsJsonArray("edgesOnCanvas"), edgeSetType));

    updateIdCounter(model.getCanvas());
    return model;
  }

  private void updateIdCounter(GroupNode canvas) {
    canvas.getAllObjectsRecursive().mapToInt(AbstractCanvasObject::getId).max()
        .ifPresent(maxId -> IdCounter.set(maxId + 1));
  }
}
