package holeg.serialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import holeg.model.AbstractCanvasObject;
import holeg.model.Edge;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class EdgeDeserializer implements JsonDeserializer<Edge> {

  public Map<Integer, AbstractCanvasObject> idMap = new HashMap<>();


  @Override
  public Edge deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {
    JsonObject jsonObj = json.getAsJsonObject();
    Edge edge = new Edge(idToCanvasObject(jsonObj.get("idA").getAsInt()),
        idToCanvasObject(jsonObj.get("idB").getAsInt()),
        jsonObj.get("maxCapacity").getAsFloat());
    edge.mode = Edge.EdgeMode.valueOf(jsonObj.get("mode").getAsString());
    return edge;
  }

  private AbstractCanvasObject idToCanvasObject(int id) throws JsonParseException {
    AbstractCanvasObject object = idMap.get(id);
    if (object == null) {
      throw new JsonParseException("Cannot find AbstractCanvasObject with id: " + id);
    }
    return object;
  }
}
