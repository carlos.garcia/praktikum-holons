package holeg.preferences;

public class ImagePreference {

  public static final String Logo = "/images/logo/holeg.png";

  public static class Canvas {

    public static final String GroupNode = "/images/canvas/group_node.png";
    public static final String ImageNotFound = "/images/canvas/image_not_found.png";
    public static final String ReplaceSymbol = "/images/canvas/replace.png";
    public static final String ExternSymbol = "/images/canvas/extern.png";

    public static class Switch {

      public static final String Open = "/images/canvas/switch_open.png";
      public static final String Closed = "/images/canvas/switch_closed.png";
    }

    public static class Node {

      public static final String Selected = "/images/canvas/node_selected.png";
      public static final String Unselected = "/images/canvas/node_unselected.png";
    }

    public static class DefaultObject {

      public static final String House = "/images/canvas/house.png";
      public static final String PowerPlant = "/images/canvas/power_plant.png";
    }

  }

  public static class Button {

    public static final String Settings = "/images/buttons/settings.png";

    public static class Console {

      public static final String Clear = "/images/buttons/clear.png";
      public static final String Top = "/images/buttons/top.png";
      public static final String Bottom = "/images/buttons/bottom.png";
    }

    public static class Inspector {

      public static final String Reset = "/images/buttons/reset_circle.png";
      public static final String Graph = "/images/buttons/graph.png";
      public static final String Add = "images/buttons/plus.png";
      public static final String Remove = "images/buttons/minus.png";
      public static final String Duplicate = "images/buttons/duplicate.png";

    }

    public static class Menu {

      public static final String Algo = "/images/buttons/algo.png";
      public static final String Outliner = "/images/buttons/outliner.png";
    }

    public static class TimePanel {

      public static final String Play = "/images/buttons/play.png";
      public static final String Pause = "/images/buttons/pause.png";
      public static final String Reset = "/images/buttons/reset.png";
      public static final String Forward = "/images/buttons/forward.png";
      public static final String Backward = "/images/buttons/backward.png";
    }

    public static class Outliner {

      public static final String Open = "/images/buttons/opened.png";
      public static final String Closed = "/images/buttons/closed.png";
      public static final String Leaf = "/images/buttons/leaf.png";
    }

    public static class GroupNode {

      public static final String Close = "/images/buttons/close_button.png";
    }
  }

  public static class Category {

    public static final String Folder = "/images/buttons/folder.png";
    public static final String Add = "/images/buttons/plus.png";
    public static final String Delete = "/images/buttons/minus.png";
  }

  public static class HolonInformationPanel {

    public static final String Filter = "/images/buttons/down.png";
    public static final String FilterHovered = "/images/buttons/down_hovered.png";
    public static final String FilterPressed = "/images/buttons/down_pressed.png";
  }
}
