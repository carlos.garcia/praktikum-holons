package holeg.preferences;

public class PreferenceKeys {

  public static class Gui {

    public static final String DefaultFolder = "DefaultFolder";
    public static final String XPos = "XPos";
    public static final String YPos = "YPos";
    public static final String Width = "Width";
    public static final String Height = "Height";
  }

  public static class Appearance {

    public static final String SupplyBarVisible = "SupplyBarVisible";
    public static final String EdgeCapacityVisible = "EdgeCapacityVisible";
    public static final String CanvasObjectEnergyVisible = "CanvasObjectEnergyVisible";
  }

  public static class EmailNotification {

    public static final String Hostname = "Hostname";
    public static final String Port = "Port";
    public static final String Username = "Username";
    public static final String Password = "Password";
    public static final String FromEmail = "FromEmail";
    public static final String ToEmail = "ToEmail";
  }

  public static class Category {

    public static final String DefaultCategory = "DefaultCategory";
  }
}
