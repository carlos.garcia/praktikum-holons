package holeg.preferences;

import holeg.model.Flexibility.FlexState;
import holeg.model.HolonObject.HolonObjectState;
import java.awt.Color;

/**
 * A Class to save all colors in Holeg.
 *
 * @author Tom
 */
public class ColorPreference {


  public static class Element {

    public static final Color Active = new Color(255, 203, 35);
    public static final Color Inactive = new Color(128, 154, 163);

    public static class Priority {

      public static final Color Essential = new Color(201, 44, 74);
      public static final Color High = new Color(255, 92, 26);
      public static final Color Medium = new Color(255, 216, 57);
      public static final Color Low = new Color(255, 255, 175);
    }
  }

  public static class HolonObject {

    public static final Color Producer = new Color(149, 152, 159, 211);
    public static final Color OverSupplied = new Color(166, 78, 229);
    public static final Color Supplied = new Color(13, 175, 28);
    public static final Color PartiallySupplied = new Color(255, 233, 1);
    public static final Color NotSupplied = new Color(230, 120, 100);
    public static final Color NoEnergy = new Color(239, 239, 239);

    public static Color getStateColor(HolonObjectState state) {
      return switch (state) {
        case NOT_SUPPLIED -> NotSupplied;
        case OVER_SUPPLIED -> OverSupplied;
        case PARTIALLY_SUPPLIED -> PartiallySupplied;
        case PRODUCER -> Producer;
        case SUPPLIED -> Supplied;
        case NO_ENERGY -> NoEnergy;
      };
    }
  }

  public static class Edge {

    public static final Color Working = new Color(13, 175, 28);
    public static final Color Burned = Color.red;
  }


  public static class Energy {

    public static final Color Production = new Color(61, 133, 243);
    public static final Color Consumption = new Color(255, 67, 60);
  }

  public static class Flexibility {

    public static final Color InUse = new Color(182, 238, 166);
    public static final Color OnCooldown = new Color(239, 215, 128);
    public static final Color Offered = new Color(75, 170, 72);
    public static final Color NotOffered = new Color(237, 106, 90);
    public static final Color Unavailable = new Color(193, 193, 193);
    public static final Color NoFlexibility = new Color(101, 101, 101);

    public static Color getStateColor(FlexState state) {
      return switch (state) {
        case IN_USE -> InUse;
        case NOT_OFFERED -> NotOffered;
        case OFFERED -> Offered;
        case ON_COOLDOWN -> OnCooldown;
        case UNAVAILABLE -> Unavailable;
      };
    }
  }

  public static class Panel {

    public static final Color Transparent = new Color(0, 0, 0, 0);
    public static final Color Background = new Color(250, 250, 250);
    public static final Color Title = new Color(54, 73, 78);
  }

  public static class Canvas {

    public static final Color MouseSelectionBorder = new Color(0, 120, 215);
    public static final Color MouseSelectionFill = new Color(128, 174, 247, 40);
    public static final Color ObjectSelectionBorder = new Color(153, 209, 255);
    public static final Color ObjectSelectionFill = new Color(205, 233, 255);
    public static final Color HolonObjectEnergy = new Color(26, 26, 26);

  }

  public static class InformationPanel {

    public static final Color NoData = new Color(243, 243, 243, 255);
  }

  public static class Dialog {

    public static final Color BackgroundColor = new Color(255, 50, 50);
  }

  public static class Category {

    public final static Color Focus = new Color(219, 245, 255);
  }

  public static class Inspector {

    public final static Color Selected = new Color(126, 186, 255);
    public final static Color Border = new Color(171, 173, 179);
  }

  public static class TimePanel {

    public static final Color Invalid = new Color(255, 192, 192);
  }

  public static class UnitGraph {

    public static final Color DotColor = Color.blue;
    public static final Color EditDotColor = new Color(255, 119, 0);
    public static final Color[] SeriesColorArray = {Color.blue, Color.cyan, Color.black,
        Color.green, Color.gray,
        Color.magenta, Color.yellow, Color.PINK, Color.red};
    public static final Color GlobalCurveColor = new Color(255, 30, 30);
    public static final Color ZeroLineColor = new Color(34, 255, 0);
  }
}
