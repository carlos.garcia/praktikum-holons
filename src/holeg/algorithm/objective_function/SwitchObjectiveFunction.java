package holeg.algorithm.objective_function;

import holeg.model.Holon;
import holeg.model.HolonObject;
import holeg.model.Model;

public class SwitchObjectiveFunction {

  //weights
  static double w_eb = .5, w_state = .5;
  static double k_eb = 1050000.f, k_state = 10000;

  static double squash_subtract = 1.0f / (1.f + (float) Math.exp(5.0));

  static public float getFitnessValueForState(Model model) {
    double f_eb = 0;
    double f_state = 0;
    double elementCountInNetwork = model.getAllHolonElements().size();
    //sum over all objects
    for (Holon holon : model.holons) {

      //weigt
      double w_network = holon.getAmountOfElements() / elementCountInNetwork;

      //f_eb
      double netEnergyDifference = holon.holonObjects.stream()
          .map(hO -> Math.abs(hO.getActualEnergy())).reduce(0.0f, Float::sum);
      //abs
      f_eb += w_network * Math.abs(netEnergyDifference);

      //f_state
      f_state += w_network * holon.holonObjects.stream().filter(HolonObject::isConsumer)
          .map(con -> supplyPenalty(con.getSupplyBarPercentage())).reduce(0., Double::sum);
    }
    return (float) (w_eb * squash(f_eb, k_eb) + w_state * squash(f_state, k_state));
  }


  /**
   * f_sup in paper
   *
   * @param supply from 0 to 1
   */
  static public double supplyPenalty(double supply) {
    double supplyPercentage = 100 * supply;
    return (supplyPercentage < 100) ? -0.5 * supplyPercentage + 50 : supplyPercentage - 100;
  }

  /**
   * The squashing function in paper
   *
   * @param x     the input
   * @param kappa the corresponding kappa
   */
  static public double squash(double x, double kappa) {
    return 100.f / (1.0f + Math.exp(-(10.f * (x - kappa / 2.f)) / kappa)) - squash_subtract;
  }
}
