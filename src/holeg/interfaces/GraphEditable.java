package holeg.interfaces;

import holeg.utility.math.vector.Vec2f;
import java.util.LinkedList;

/**
 * Interface for all Elements that have a Graph to edit it state over time.
 *
 * @author Tom Troppmann
 */


public interface GraphEditable {

  /**
   * Determine what type the Graph have.
   *
   * @return the type of the Graph
   */
  GraphType getGraphType();

  /**
   * Getter for the graph.
   *
   * @return The list of all graph points.
   */
  LinkedList<Vec2f> getStateGraph();

  /**
   * Sample the Graph on the object.
   */
  void sampleGraph();

  /**
   * Resets the Graph two the initial start e.g. the Point Left and Right at 100%
   */
  void reset();

  /**
   * all types of graphs
   */
  enum GraphType {
    boolGraph, doubleGraph
  }
}
