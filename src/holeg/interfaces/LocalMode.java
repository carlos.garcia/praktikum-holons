package holeg.interfaces;

public interface LocalMode {

  int STANDARD_GRAPH_ACCURACY = 100;

  Period getPeriod();

  /**
   * Determine if it should use his own LocalPeriod or use the global Period over the entire
   * simulation. Local Period is opposed to repeated once the period is over.
   */
  void setPeriod(Period period);

  class Period {

    private final PeriodType type;
    private int interval;

    public Period(Period other) {
      this.type = other.type;
      this.interval = other.interval;
    }
    public Period(int interval) {
      this.interval = interval;
      type = PeriodType.Local;
    }

    public Period() {
      type = PeriodType.Global;
    }

    /**
     * Used to query the local period of an element.
     *
     * @return The local period of this element. This should return the set local period, which may
     * be anything, even if the component is set to be "stretching", that is, acting as if the local
     * period was the same as the number of total iterations for this simulation.
     */
    public int getInterval() {
      if (type == PeriodType.Local) {
        return interval;
      }
      return STANDARD_GRAPH_ACCURACY;
    }

    ;

    /**
     * Sets the local period of the element. If the simulation has 100 steps and the local period is
     * 50, then there will be two full cycles during the simulation.
     *
     * @param interval The local period for this element.
     */
    public void setInterval(int interval) {
      this.interval = interval;
    }

    public Period.PeriodType getType() {
      return type;
    }

    public enum PeriodType {
      Local, Global
    }
  }
}
