package holeg.addon;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import holeg.addon.helper.HolonElementSketch;
import holeg.addon.helper.RandomPriotity;
import holeg.api.AddOn;
import holeg.model.AbstractCanvasObject;
import holeg.model.HolonElement;
import holeg.model.HolonObject;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.view.image.Import;
import holeg.utility.math.Random;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;

public class Randomizer implements AddOn {

  private final JPanel content = new JPanel();
  private final JPanel tablePanel = new JPanel();
  private final RandomPriotity prioPanel = new RandomPriotity();
  private final JCheckBox priorityCheckbox = new JCheckBox("Random");
  private final HashMap<HolonObject, Boolean> objectMap = new HashMap<>();
  private final List<JCheckBox> checkboxList = new ArrayList<>();
  public double randomChance = 1.0;
  private Control control;
  private int minAmountOfElements = 3;
  private int maxAmountOfElements = 7;
  private boolean useRandomPriority = true;
  private File file;
  public Randomizer() {
    content.setLayout(new BorderLayout());
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, createParameterPanel(),
        createFilterPanel());
    splitPane.setDividerLocation(0.5);
    content.add(splitPane, BorderLayout.CENTER);

    JButton buttonRun = new JButton("Run");
    buttonRun.addActionListener(actionEvent -> run());
    content.add(buttonRun, BorderLayout.PAGE_END);
  }

  //To Test the Layout Faster
  public static void main(String[] args) {
    JFrame newFrame = new JFrame("exampleWindow");
    Randomizer instance = new Randomizer();
    newFrame.setContentPane(instance.getPanel());
    newFrame.pack();
    newFrame.setVisible(true);
    newFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private JScrollPane createFilterPanel() {
    //Table

    tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.PAGE_AXIS));
    JScrollPane scrollPanel = new JScrollPane(tablePanel);
    scrollPanel.setPreferredSize(new Dimension(300, 300));

    fillTablePanel();
    return scrollPanel;
  }

  private void fillTablePanel() {
    //Clear old Data
    tablePanel.removeAll();
    checkboxList.clear();

    //HeadPanel
    int lineSpace = 20;
    JPanel headPanel = new JPanel();
    headPanel.setLayout(new BoxLayout(headPanel, BoxLayout.LINE_AXIS));
    headPanel.add(new JLabel("FILTER"));
    JButton updateButton = new JButton();
    updateButton.setIcon(
        new ImageIcon(Import.loadImage(ImagePreference.Canvas.ReplaceSymbol, 15, 15)));
    updateButton.addActionListener(action -> {
      this.updateFilterList();
      content.updateUI();
    });
    updateButton.setToolTipText("Manuel updates the filter list with all canvas objects.");
    headPanel.add(updateButton);
    headPanel.add(Box.createHorizontalGlue());
    headPanel.add(new JLabel("SelectAll"));
    JCheckBox selectAllCheckbox = new JCheckBox();
    selectAllCheckbox.setSelected(true);
    selectAllCheckbox.addItemListener(
        input -> checkboxList.forEach(box -> box.setSelected(selectAllCheckbox.isSelected())));
    headPanel.add(selectAllCheckbox);
    tablePanel.add(headPanel);

    //Entrys
    for (HolonObject hObject : objectMap.keySet().stream()
        .sorted(Comparator.comparing(AbstractCanvasObject::getName)).toList()) {
      //Entry
      JPanel entryPanel = new JPanel();
      entryPanel.setLayout(new BoxLayout(entryPanel, BoxLayout.LINE_AXIS));
      JLabel label = new JLabel(hObject.getName() + "[" + hObject.getId() + "]",
          new ImageIcon(Import.loadImage(hObject.getImagePath(), lineSpace, lineSpace)),
          JLabel.LEFT);
      entryPanel.add(label);
      entryPanel.add(Box.createHorizontalGlue());
      JCheckBox checkbox = new JCheckBox();
      checkbox.setSelected(true);
      checkbox.addItemListener(change -> objectMap.put(hObject, checkbox.isSelected()));
      checkboxList.add(checkbox);
      entryPanel.add(checkbox);
      tablePanel.add(entryPanel);
    }
  }


  private JSplitPane createParameterPanel() {

    JPanel choosePanel = new JPanel(null);
    choosePanel.setPreferredSize(new Dimension(300, 200));
    choosePanel.setMinimumSize(new Dimension(300, 200));
    JLabel minAmount = new JLabel("minAmountOfElements:");
    minAmount.setBounds(20, 60, 200, 20);
    choosePanel.add(minAmount);
    JLabel maxAmount = new JLabel("maxAmountOfElements:");
    maxAmount.setBounds(20, 85, 200, 20);
    choosePanel.add(maxAmount);

    //Integer formatter
    NumberFormat format = NumberFormat.getIntegerInstance();
    format.setGroupingUsed(false);
    format.setParseIntegerOnly(true);
    NumberFormatter integerFormatter = new NumberFormatter(format);
    integerFormatter.setMinimum(0);
    integerFormatter.setCommitsOnValidEdit(true);

    JFormattedTextField minAmountTextField = new JFormattedTextField(integerFormatter);
    minAmountTextField.setValue(this.minAmountOfElements);
    minAmountTextField.setToolTipText("Only positive Integer.");
    minAmountTextField.addPropertyChangeListener(
        actionEvent -> this.minAmountOfElements = Integer.parseInt(
            minAmountTextField.getValue().toString()));
    minAmountTextField.setBounds(220, 60, 50, 20);
    choosePanel.add(minAmountTextField);

    JFormattedTextField maxAmountTextField = new JFormattedTextField(integerFormatter);
    maxAmountTextField.setValue(this.maxAmountOfElements);
    maxAmountTextField.setToolTipText("Only positive Integer.");
    maxAmountTextField.addPropertyChangeListener(
        actionEvent -> this.maxAmountOfElements = Integer.parseInt(
            maxAmountTextField.getValue().toString()));
    maxAmountTextField.setBounds(220, 85, 50, 20);
    choosePanel.add(maxAmountTextField);

    JButton chooseFileButton = new JButton("Choose File");
    chooseFileButton.setBounds(20, 10, 200, 30);
    chooseFileButton.addActionListener(actionEvent -> file = openCatalogFile());
    choosePanel.add(chooseFileButton);

    JSlider bitSlider = createFlipChanceSlider();
    bitSlider.setBounds(10, 110, 280, 80);
    choosePanel.add(bitSlider);

    JPanel prioritySelectionPanel = new JPanel();
    prioritySelectionPanel.setLayout(new BoxLayout(prioritySelectionPanel, BoxLayout.PAGE_AXIS));
    //priorityCheckbox.setAlignmentY(0.0f);
    //selection
    prioritySelectionPanel.add(this.priorityCheckbox);
    priorityCheckbox.addItemListener(change -> {
      prioPanel.setEnabled(priorityCheckbox.isSelected());
      useRandomPriority = priorityCheckbox.isSelected();
    });
    priorityCheckbox.setSelected(useRandomPriority);

    prioritySelectionPanel.add(prioPanel);
    JSplitPane parameterPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, choosePanel,
        prioritySelectionPanel);
    parameterPanel.setPreferredSize(new Dimension(600, 200));

    return parameterPanel;
  }

  private void run() {
    List<HolonElementSketch> holonElementCatalog = new ArrayList<>();
    if (file == null) {
      file = openCatalogFile();
    }
    if (file == null) {
      return;
    }
    readCatalogFromJson(file, holonElementCatalog);
    holonElementCatalog.forEach(HolonElementSketch::checkValues);
    objectMap.forEach((hObject, state) -> {
      //Ignore Filtered objects
      if (!state) {
        return;
      }
      //Randomize
      hObject.clearElements();
      int randomAmountOfElementsToAdd = Random.nextIntegerInRange(minAmountOfElements,
          maxAmountOfElements + 1);
      for (int i = 0; i < randomAmountOfElementsToAdd; i++) {
        HolonElement ele = holonElementCatalog.get(
                Random.nextIntegerInRange(0, holonElementCatalog.size()))
            .createHolonElement(hObject, Random.nextDouble() < randomChance);
        if (this.useRandomPriority) {
          ele.setPriority(prioPanel.getPriority());
        }
        hObject.add(ele);
      }

    });
    control.updateStateForCurrentIteration();
  }


  public void updateFilterList() {
    objectMap.clear();
    if (control != null) {
      control.getModel().getCanvas().getAllHolonObjectsRecursive()
          .forEach(object -> objectMap.put(object, true));
    }
    this.fillTablePanel();
  }

  @Override
  public JPanel getPanel() {
    return content;
  }

  @Override
  public void setController(Control control) {
    this.control = control;

    //Update Filter List
    updateFilterList();
  }


  private JSlider createFlipChanceSlider() {
    JSlider flipChance = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
    flipChance.setBorder(BorderFactory.createTitledBorder("ActiveProbability"));
    flipChance.setMajorTickSpacing(50);
    flipChance.setMinorTickSpacing(5);
    flipChance.setPaintTicks(true);
    Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
    labelTable.put(0, new JLabel("0.0"));
    labelTable.put(50, new JLabel("0.5"));
    labelTable.put(100, new JLabel("1.0"));
    flipChance.setToolTipText("" + randomChance);
    flipChance.addChangeListener(actionEvent -> {
      randomChance = (double) flipChance.getValue() / 100.0;
      flipChance.setToolTipText("" + randomChance);
    });
    flipChance.setLabelTable(labelTable);
    flipChance.setPaintLabels(true);
    return flipChance;
  }


  private void readCatalogFromJson(File file, List<HolonElementSketch> catalog) {
    Gson gson = new Gson();
    try {
      JsonElement jsonTreeRoot = JsonParser.parseReader(new FileReader(file));
      if (jsonTreeRoot.isJsonArray()) {
        JsonArray jsonArray = jsonTreeRoot.getAsJsonArray();
        jsonArray.forEach(jsonObject -> {
          HolonElementSketch newObject = gson.fromJson(jsonObject, HolonElementSketch.class);
          catalog.add(newObject);
        });
      }
    } catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
      e.printStackTrace();
    }

  }


  public File openCatalogFile() {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(
        new File(System.getProperty("user.dir") + "/config/randomizer/"));
    fileChooser.setFileFilter(new FileNameExtensionFilter("JSON Files", "json"));
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setAcceptAllFileFilterUsed(false);
    int result = fileChooser.showOpenDialog(content);
    if (result == JFileChooser.APPROVE_OPTION) {
      //Found a file
      return fileChooser.getSelectedFile();
    }
    return null;
  }

}
