package holeg.addon.helper;

import holeg.model.Constrain;
import holeg.model.Flexibility;
import holeg.model.HolonElement;
import holeg.model.HolonElement.Priority;
import holeg.model.HolonObject;
import holeg.utility.math.Random;

public class HolonElementSketch {

  //HolonElement
  public String name;
  public int minAmount;
  public int maxAmount;
  public float energy;

  public String priority;

  public FlexibilitySketch onFlex;
  public FlexibilitySketch offFlex;

  public HolonElementSketch(String name, float energy) {
    this.name = name;
    this.energy = energy;
  }

  public HolonElement createHolonElement(HolonObject parentObject, boolean active) {
    HolonElement ele = new HolonElement(parentObject, name, energy);
    ele.active = active;
    if (onFlex != null && Random.nextDouble() < onFlex.flexChance) {
      addFlex(ele, parentObject.getName(), true);
    }
    if (offFlex != null && Random.nextDouble() < offFlex.flexChance) {
      addFlex(ele, parentObject.getName(), false);
    }
    ele.setPriority(Priority.valueOf(priority));
    return ele;
  }

  public void checkValues() {
    minAmount = Math.abs(minAmount);
    maxAmount = Math.abs(maxAmount);
    if (maxAmount < minAmount) {
      //Swap
      int intermediate = minAmount;
      minAmount = maxAmount;
      maxAmount = intermediate;
    }
    if (onFlex != null) {
      onFlex.checkValues();
    }
    if (offFlex != null) {
      offFlex.checkValues();
    }
  }

  public void addFlex(HolonElement ele, String nameOfHolonObject, boolean onConstrain) {
    Flexibility toCreateFlex = new Flexibility(ele);
    FlexibilitySketch constrain = onConstrain ? onFlex : offFlex;

    toCreateFlex.name =
        nameOfHolonObject + "_" + ele.getName() + (onConstrain ? "_OnFlex" : "_OffFlex");
    toCreateFlex.speed = 0;
    toCreateFlex.setDuration(
        Random.nextIntegerInRange(constrain.minDuration, constrain.maxDuration + 1));
    toCreateFlex.cost = Random.nextFloatInRange(constrain.minCost, constrain.maxCost);
    toCreateFlex.setCooldown(
        Random.nextIntegerInRange(constrain.minCooldown, constrain.maxCooldown + 1));
    toCreateFlex.offered = true;
    if (onConstrain) {
      toCreateFlex.constrainList.add(Constrain.createOnConstrain());
    } else {
      toCreateFlex.constrainList.add(Constrain.createOffConstrain());
    }
    ele.flexList.add(toCreateFlex);
  }
}