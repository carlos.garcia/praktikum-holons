package holeg.addon.helper;

import holeg.preferences.ImagePreference;
import holeg.preferences.PreferenceKeys;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;


public class EmailNotification {

  private static final Preferences prefs = Preferences.userNodeForPackage(EmailNotification.class);
  private static final EmailSmtpInformation info = new EmailSmtpInformation();

  public static void main(String[] args) {
    OpenEmailSettings(null);
  }

  public static void OpenEmailSettings(JPanel parent) {
    loadPreferences();
    JFrame frame = new JFrame();
    frame.setTitle("Email Settings");
    frame.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    frame.setContentPane(createEditFormular(frame));
    frame.pack();
    frame.setPreferredSize(new Dimension(400, frame.getHeight()));
    frame.pack();
    frame.setVisible(true);
    frame.setLocationRelativeTo(parent);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  }

  private static JPanel createEditFormular(JFrame frame) {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    panel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

    JPanel infoPanel = new JPanel();
    GridLayout infoLayout = new GridLayout(0, 2);
    infoPanel.add(new JLabel("SMTP Hostname:"));
    JTextField hostnameTextField = new JTextField(info.Hostname);
    infoPanel.add(hostnameTextField);
    infoPanel.add(new JLabel("Port:"));
    JTextField portTextField = new JTextField(Integer.toString(info.Port));
    infoPanel.add(portTextField);
    infoPanel.add(new JLabel("Username:"));
    JTextField usernameTextField = new JTextField(info.Username);
    infoPanel.add(usernameTextField);
    infoPanel.add(new JLabel("Password:"));
    JPasswordField passwordTextField = new JPasswordField(info.Password);
    infoPanel.add(passwordTextField);
    infoPanel.add(new JLabel("From Email:"));
    JTextField fromEmailTextField = new JTextField(info.FromEmail);
    infoPanel.add(fromEmailTextField);
    infoPanel.add(new JLabel("To Email:"));
    JTextField toEmailTextField = new JTextField(info.ToEmail);
    infoPanel.add(toEmailTextField);
    infoPanel.setLayout(infoLayout);
    panel.add(infoPanel, BorderLayout.CENTER);

    JPanel controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    controlPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(event -> frame.dispose());
    controlPanel.add(cancelButton);
    JButton applyButton = new JButton("Apply and Close");
    applyButton.addActionListener(event -> {
      // parse Textfields
      info.Hostname = hostnameTextField.getText();
      info.Port = Integer.parseInt(portTextField.getText());
      info.Username = usernameTextField.getText();
      info.Password = String.valueOf(passwordTextField.getPassword());
      info.FromEmail = fromEmailTextField.getText();
      info.ToEmail = toEmailTextField.getText();
      // Save Preferences
      savePreferences();
      frame.dispose();
    });
    controlPanel.add(applyButton);

    JButton testButton = new JButton("Send Test-Email");
    testButton.addActionListener(event -> {
      // parse Textfields
      EmailSmtpInformation testInfo = new EmailSmtpInformation();
      testInfo.Hostname = hostnameTextField.getText();
      testInfo.Port = Integer.parseInt(portTextField.getText());
      testInfo.Username = usernameTextField.getText();
      testInfo.Password = String.valueOf(passwordTextField.getPassword());
      testInfo.FromEmail = fromEmailTextField.getText();
      testInfo.ToEmail = toEmailTextField.getText();
      sendEmail(testInfo, "Holeg Notification Test", "Success.");
    });
    controlPanel.add(testButton);

    panel.add(controlPanel, BorderLayout.PAGE_END);

    return panel;
  }

  public static void sendEmail(String subject, String message) {
    loadPreferences();
    sendEmail(info, subject, message);
  }

  public static void sendEmail(EmailSmtpInformation info, String subject, String message) {
    Email email = new SimpleEmail();
    email.setHostName(info.Hostname);
    email.setSmtpPort(info.Port);
    email.setAuthenticator(new DefaultAuthenticator(info.Username, info.Password));
    email.setSSLOnConnect(true);
    email.setSubject(subject);
    try {
      email.setFrom(info.FromEmail);
      email.setMsg(message);
      email.addTo(info.ToEmail);
      email.send();
    } catch (EmailException e) {
      e.printStackTrace();
    }
  }

  private static void savePreferences() {
    prefs.put(PreferenceKeys.EmailNotification.Hostname, info.Hostname);
    prefs.putInt(PreferenceKeys.EmailNotification.Port, info.Port);
    prefs.put(PreferenceKeys.EmailNotification.Username, info.Username);
    prefs.put(PreferenceKeys.EmailNotification.Password, info.Password);
    prefs.put(PreferenceKeys.EmailNotification.FromEmail, info.FromEmail);
    prefs.put(PreferenceKeys.EmailNotification.ToEmail, info.ToEmail);
  }

  private static void loadPreferences() {
    info.Hostname = prefs.get(PreferenceKeys.EmailNotification.Hostname, "");
    info.Port = prefs.getInt(PreferenceKeys.EmailNotification.Port, 465);
    info.Username = prefs.get(PreferenceKeys.EmailNotification.Username, "");
    info.Password = prefs.get(PreferenceKeys.EmailNotification.Password, "");
    info.FromEmail = prefs.get(PreferenceKeys.EmailNotification.FromEmail, "");
    info.ToEmail = prefs.get(PreferenceKeys.EmailNotification.ToEmail, "");
  }

  public static class EmailSmtpInformation {

    public String Hostname;
    public int Port;
    public String Username;
    public String Password;
    public String FromEmail;
    public String ToEmail;
  }


}
