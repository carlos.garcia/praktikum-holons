package holeg.model;

import java.util.function.Predicate;

public class Constrain {

  /**
   * Flexibility should be offered when Element is active.
   */
  public static Predicate<Flexibility> onConstrain = f -> f.getElement().active;
  /**
   * Flexibility should be offered when Element is inactive.
   */
  public static Predicate<Flexibility> offConstrain = f -> !f.getElement().active;
  private final Predicate<Flexibility> constrainFunction;
  private final String name;

  public Constrain(Predicate<Flexibility> constrainFunction, String name) {
    this.constrainFunction = constrainFunction;
    this.name = name;
  }

  //Example Constrains:

  public static Constrain createOnConstrain() {
    return new Constrain(onConstrain, "onConstrain");
  }

  public static Constrain createOffConstrain() {
    return new Constrain(offConstrain, "offConstrain");
  }

  public Predicate<Flexibility> getConstrainFunction() {
    return constrainFunction;
  }

  public String getName() {
    return name;
  }

}
