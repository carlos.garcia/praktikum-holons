package holeg.model;

/**
 * The class "CpsNode" represents empty Objects in the system. They are just nodes between Objects
 *
 * @author Gruppe14
 */
public class Node extends AbstractCanvasObject {

  public Node(String objName) {
    super(objName);
  }

  public Node(Node node) {
    super(node);
  }

  @Override
  public AbstractCanvasObject copy() {
    return new Node(this);
  }

  public String toString() {
    return "Node ID:" + getId();
  }

}
