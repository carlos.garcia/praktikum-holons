package holeg.model;

import holeg.interfaces.LocalMode;
import holeg.interfaces.TimelineDependent;
import holeg.preferences.ImagePreference;
import holeg.serialize.PostDeserialize;
import holeg.ui.controller.IndexTranslator;
import holeg.utility.math.vector.Vec2f;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * The class HolonSwitch represents an Object in the system, that has the capacity of manipulate the
 * electricity flow. The switch can be manage automatically through a graph or direct manually.
 *
 * @author Gruppe14
 */
public class HolonSwitch extends AbstractCanvasObject implements TimelineDependent,
    PostDeserialize {

  /**
   * Energy at each point of the graph with 50 predefined points. At the beginning, it starts with
   * all values at energy
   */
  boolean[] activeAt = new boolean[LocalMode.STANDARD_GRAPH_ACCURACY];
  /**
   * Points on the UnitGraph
   */
  LinkedList<Vec2f> graphPoints = new LinkedList<>();
  private SwitchMode mode = SwitchMode.Auto;
  private SwitchState manualState = SwitchState.Closed;
  private SwitchState state = SwitchState.Closed;
  private Period period = new Period();

  /**
   * Create a new HolonSwitch with a custom name, a default value of automatic handle and active
   * status.
   *
   * @param name String
   */
  public HolonSwitch(String name) {
    super(name);
    initGraphPoints();
    sampleGraph();
  }


  /**
   * Create a copy of an existing HolonSwitch.
   *
   * @param other the Object to copy
   */
  public HolonSwitch(HolonSwitch other) {
    super(other);
    mode = other.mode;
    manualState = other.manualState;
    state = other.state;
    setPeriod(other.getPeriod());
    activeAt = new boolean[LocalMode.STANDARD_GRAPH_ACCURACY];
    setName(other.getName());
    for (Vec2f p : other.getGraphPoints()) {
      this.graphPoints.add(new Vec2f(p.getX(), p.getY()));
    }
    sampleGraph();
  }

  @Override
  public AbstractCanvasObject copy() {
    return new HolonSwitch(this);
  }


  @Override
  public String getImagePath() {
    return switch (state) {
      case Open -> ImagePreference.Canvas.Switch.Open;
      case Closed -> ImagePreference.Canvas.Switch.Closed;
    };
  }

  /**
   * Calculates the state of the Switch.
   */
  public void flipManualState() {
    manualState = SwitchState.opposite(manualState);
  }

  public void calculateState(int timestep) {
    switch (mode) {
      case Auto:
        state = activeAt[IndexTranslator.getEffectiveIndex(this, timestep)] ? SwitchState.Open
            : SwitchState.Closed;
      case Manual:
      default:
        state = manualState;
    }
  }

  /*
   * STATE
   */

  public SwitchState getState() {
    return state;
  }

  /**
   * For automatic use only (through the graph).
   *
   * @return the Graph Points
   */
  public LinkedList<Vec2f> getGraphPoints() {
    return graphPoints;
  }

  /**
   * Set the values of the switch in the graph (auto. mode only).
   *
   * @param linkedList the Graph points
   */
  public void setGraphPoints(LinkedList<Vec2f> linkedList) {
    this.graphPoints = linkedList;
  }

  /**
   * Initialize the Graph as a closed Switch.
   */
  private void initGraphPoints() {
    graphPoints.clear();
    graphPoints.add(new Vec2f(0, 1));
    graphPoints.add(new Vec2f(1, 1));
  }

  public SwitchMode getMode() {
    return mode;
  }

  public void setMode(SwitchMode mode) {
    this.mode = mode;
  }

  public SwitchState getManualState() {
    return manualState;
  }

  public void setManualState(SwitchState manuelState) {
    this.manualState = manuelState;
  }

  // interfaces.GraphEditable
  @Override
  public GraphType getGraphType() {
    return GraphType.boolGraph;
  }

  @Override
  public LinkedList<Vec2f> getStateGraph() {
    return graphPoints;
  }

  @Override
  public void reset() {
    initGraphPoints();
    sampleGraph();
  }

  @Override
  public void sampleGraph() {
    activeAt = sampleGraph(100);
  }

  /**
   * Generate out of the Graph Points a array of boolean that represent the Curve("on or off"-Graph)
   * at each sample position. The Values are in the Range [0,1].
   *
   * @param sampleLength amount of samplePositions. The positions are equidistant on the
   *                     Range[0,1].
   * @return the boolean array of samplepoints.
   */
  private boolean[] sampleGraph(int sampleLength) {
    ListIterator<Vec2f> iter = this.graphPoints.listIterator();
    Vec2f before = iter.next();
    Vec2f after = iter.next();
    boolean[] activeTriggerPos = new boolean[sampleLength];
    for (int i = 0; i < sampleLength; i++) {
      double graphX = (double) i / (double) (sampleLength - 1); // from 0.0 to 1.0
      if (graphX > after.getX()) {
        before = after;
        after = iter.next();
      }
      activeTriggerPos[i] = (before.getY() >= 0.5);
    }
    return activeTriggerPos;
  }

  @Override
  public Period getPeriod() {

    return period;
  }

  @Override
  public void setPeriod(Period period) {
    this.period = period;
  }

  public String toString() {
    return name + "[ID:" + getId() + "]";

  }

  @Override
  public void postDeserialize() {
    sampleGraph();
  }

  public boolean isOpen() {
    return getState().isOpen();
  }

  public boolean isClosed() {
    return getState().isClosed();
  }

  public enum SwitchState {
    Open, Closed;

    public static SwitchState opposite(SwitchState state) {
      return switch (state) {
        case Closed -> Open;
        case Open -> Closed;
      };
    }

    public boolean isOpen() {
      return this == Open;
    }

    public boolean isClosed() {
      return this == Closed;
    }
  }


  public enum SwitchMode {
    Manual, Auto
  }
}