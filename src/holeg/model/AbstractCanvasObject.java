package holeg.model;

import holeg.ui.model.IdCounter;
import holeg.utility.math.vector.Vec2i;
import java.util.Optional;

/**
 * The abstract class "CpsObject" represents any possible object in the system (except Edges). The
 * representation of any object contains following variables: see description of variables
 *
 * @author Gruppe14
 */
public abstract class AbstractCanvasObject {

  /* ID of the Obj. */
  private final int id;
  /* Name given by the user. */
  String name = "";
  /* Path of the image for the Obj. */
  String imagePath = "";
  /* Position with a X and Y value */
  Vec2i position = new Vec2i(0, 0);
  private transient GroupNode groupNode;

  /**
   * Constructor for a CpsObejct with an unique ID.
   *
   * @param objName of the Object
   */
  public AbstractCanvasObject(String objName) {
    setName(objName);
    this.id = IdCounter.next();
  }

  /**
   * Constructor for a new CpsObject with an unique ID (This constructor correspond to the
   * interaction between the Categories and Canvas)--> actually the "new" Object is a copy.
   *
   * @param other Object to be copied
   */
  public AbstractCanvasObject(AbstractCanvasObject other) {
    setName(other.getName());
    setImagePath(other.getImagePath());
    this.position = new Vec2i(other.position);
    this.id = IdCounter.next();

  }

  public Optional<GroupNode> getGroupNode() {
    return Optional.ofNullable(groupNode);
  }

  public void setGroupNode(GroupNode groupNode) {
    this.groupNode = groupNode;
  }


  public abstract AbstractCanvasObject copy();

  /**
   * Getter for the user-defined name (no unique).
   *
   * @return String
   */
  public String getName() {
    return name;
  }

  /**
   * Set the name.
   *
   * @param name String
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Get the path of the image for the selected Object.
   *
   * @return String
   */
  public String getImagePath() {
    return imagePath;
  }

  /**
   * Set the path of the image.
   *
   * @param imagePath the Image to set
   */
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public int getId() {
    return id;
  }


  /**
   * Set the position of the Object in the canvas.
   *
   * @param x X-Coord
   * @param y Y-Coord
   */
  public void setPosition(int x, int y) {
    setPosition(new Vec2i(x, y));
  }

  /**
   * Get the actual position of the Object.
   *
   * @return Position Position of this Object
   */
  public Vec2i getPosition() {
    return position;
  }

  /**
   * Set the position of the Object in the canvas.
   *
   * @param pos Coordinates
   */
  public void setPosition(Vec2i pos) {
    this.position = pos;
  }
}
