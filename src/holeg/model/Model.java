package holeg.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/**
 * The Class Model is the class where everything is saved. All changes made to the Data is managed
 * via a controller.
 *
 * @author Gruppe14
 */
public class Model {

  private static final Logger log = Logger.getLogger(Model.class.getName());
  public transient Set<Holon> holons = new HashSet<>();
  private GroupNode canvas = new GroupNode("Canvas");
  /**
   * the amount of iterations
   */
  private int currentIteration = 0;
  private int maxIterations = 100;

  private Set<Edge> edgesOnCanvas = new HashSet<>();

  public Model() {
    log.fine("Init Model");
  }

  public GroupNode getCanvas() {
    return canvas;
  }

  public void setCanvas(GroupNode canvas) {
    this.canvas = canvas;
  }

  public Set<Edge> getEdgesOnCanvas() {
    return edgesOnCanvas;
  }

  public void setEdgesOnCanvas(Set<Edge> edgesOnCanvas) {
    this.edgesOnCanvas = edgesOnCanvas;
  }

  public void addEdgeOnCanvas(Edge edge) {
    this.edgesOnCanvas.add(edge);
  }

  public void removeEdgesOnCanvas(Edge edge) {
    this.edgesOnCanvas.remove(edge);
  }

  public int getMaxIterations() {
    return maxIterations;
  }

  public int getCurrentIteration() {
    return currentIteration;
  }

  public void setCurrentIteration(int value) {
    this.currentIteration = value;
  }

  public List<HolonElement> getAllHolonElements() {
    return canvas.getAllHolonObjectsRecursive().flatMap(HolonObject::elementsStream)
        .collect(Collectors.toList());
  }

  public List<Flexibility> getAllFlexibilities() {
    return canvas.getAllHolonObjectsRecursive()
        .flatMap(hO -> hO.elementsStream().flatMap(ele -> ele.flexList.stream()))
        .collect(Collectors.toList());
  }

  public void reset() {
    resetFlexibilities();
    resetEdges();
  }

  private void resetFlexibilities() {
    getAllFlexibilities().forEach(Flexibility::reset);
  }

  private void resetEdges() {
    this.getEdgesOnCanvas().forEach(Edge::reset);
  }

  public void setIterations(int iterations) {
    this.maxIterations = iterations;
  }

  public void clear() {
    this.edgesOnCanvas.clear();
    canvas.clear();
  }


}
