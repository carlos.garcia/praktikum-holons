package holeg.model;


public class Edge {

  // Max. capacity of the Edge, if flow is greater than the status --> is
  // Working would be false
  public float maxCapacity;
  public EdgeMode mode = EdgeMode.Normal;
  // Source
  AbstractCanvasObject a;
  // Destination
  AbstractCanvasObject b;
  private transient EdgeState state = initState();
  private transient float flowEnergy = 0;

  /**
   * Constructor with a user-defined max. capacity
   *
   * @param a      Source
   * @param b      Destination
   * @param maxCap Maximum Capacity
   */
  public Edge(AbstractCanvasObject a, AbstractCanvasObject b, float maxCap) {
    setA(a);
    setB(b);
    this.maxCapacity = maxCap;
  }


  /**
   * Clone Constructor
   */
  public Edge(Edge other) {
    setA(other.a);
    setB(other.b);
    this.maxCapacity = other.maxCapacity;
    this.state = other.state;
    this.mode = other.mode;
    this.flowEnergy = other.flowEnergy;
  }

  /**
   * Getter for the length of the Cable.
   */
  public float getLength() {
    return a.getPosition().getDistance(b.getPosition());
  }

  /**
   * Getter for the Source.
   *
   * @return the a
   */
  public AbstractCanvasObject getA() {
    return a;
  }

  /**
   * Set the Source to a new one.
   *
   * @param a the a to set
   */
  public void setA(AbstractCanvasObject a) {
    this.a = a;
  }

  /**
   * Getter for the destination.
   *
   * @return the b
   */
  public AbstractCanvasObject getB() {
    return b;
  }

  /**
   * Set the Destination to a new one.
   *
   * @param b the b to set
   */
  public void setB(AbstractCanvasObject b) {
    this.b = b;
  }

  /**
   * Check if a CpsEdge is Connected to the AbstractCpsObject.
   *
   * @param holonObject the AbstractCpsObject to check.
   * @return true if either a or b is the AbstractCpsObject to check.
   */
  public boolean isConnectedTo(AbstractCanvasObject holonObject) {
    return (holonObject.equals(a) || holonObject.equals(b));
  }

  @Override
  public String toString() {
    String A = (a == null) ? "null" : a.getName() + " [" + a.getId() + "]";
    String B = (b == null) ? "null" : b.getName() + " [" + b.getId() + "]";
    return "Edge: " + A + " to " + B;
  }

  public EdgeState getState() {
    return state;
  }

  public void setState(EdgeState state) {
    this.state = state;
  }

  void reset() {
    state = initState();
  }

  private EdgeState initState() {
    if (mode == EdgeMode.Disconnected) {
      return EdgeState.Burned;
    }
    return EdgeState.Working;
  }

  public float getActualFlow() {
    return flowEnergy;
  }

  public void setActualFlow(float energyToSupplyInTheNetwork) {
    flowEnergy = energyToSupplyInTheNetwork;
  }

  public float getEnergyFromConneted() {
    float energy = 0.0f;
    if (getA() instanceof HolonObject hO && hO.getActualEnergy() > 0) {
      energy += hO.getActualEnergy();
    }
    if (getB() instanceof HolonObject hO && hO.getActualEnergy() > 0) {
      energy += hO.getActualEnergy();
    }
    return energy;
  }

  public enum EdgeMode {
    Normal, Unlimited, Disconnected
  }

  /*
   * STATE
   */
  public enum EdgeState {
    Working, Burned
  }
}