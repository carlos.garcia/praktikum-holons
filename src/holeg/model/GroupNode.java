package holeg.model;

import holeg.preferences.ImagePreference;
import holeg.serialize.PostDeserialize;
import holeg.utility.math.vector.Vec2i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class GroupNode extends AbstractCanvasObject implements PostDeserialize {

  private static final Logger log = Logger.getLogger(AbstractCanvasObject.class.getName());

  protected final List<HolonObject> objectList = new ArrayList<>();
  protected final List<HolonSwitch> switchList = new ArrayList<>();
  protected final List<Node> nodeList = new ArrayList<>();
  protected final List<GroupNode> groupNodeList = new ArrayList<>();

  public GroupNode(String nodeName) {
    super(nodeName);
  }

  public GroupNode(GroupNode other) {
    super(other.name);
    for (HolonObject obj : other.objectList) {
      this.add(new HolonObject(obj));
    }
    for (HolonSwitch sw : other.switchList) {
      this.add(new HolonSwitch(sw));
    }
    for (Node node : other.nodeList) {
      this.add(new Node(node));
    }
    for (GroupNode groupNode : other.groupNodeList) {
      this.add(new GroupNode(groupNode));
    }
  }

  public static Vec2i calculateMiddlePosition(Collection<AbstractCanvasObject> objects) {
    Vec2i middle = new Vec2i();
    if (!objects.isEmpty()) {
      objects.forEach(obj -> middle.addAssign(obj.getPosition()));
      middle.divideAssign(objects.size());
    }
    return middle;
  }

  @Override
  public AbstractCanvasObject copy() {
    return new GroupNode(this);
  }

  @Override
  public String getImagePath() {
    return ImagePreference.Canvas.GroupNode;
  }

  public void addAll(Stream<AbstractCanvasObject> stream) {
    stream.forEach(this::add);
  }

  public void addAll(Collection<AbstractCanvasObject> collection) {
    collection.forEach(this::add);
  }

  public void add(AbstractCanvasObject object) {
    if (object instanceof HolonObject hObject) {
      objectList.add(hObject);
    } else if (object instanceof HolonSwitch hSwitch) {
      switchList.add(hSwitch);
    } else if (object instanceof Node node) {
      nodeList.add(node);
    } else if (object instanceof GroupNode groupNode) {
      groupNodeList.add(groupNode);
    }
    object.setGroupNode(this);
  }

  public void remove(AbstractCanvasObject object) {
    if (object instanceof HolonObject hObject) {
      objectList.remove(hObject);
    } else if (object instanceof HolonSwitch hSwitch) {
      switchList.remove(hSwitch);
    } else if (object instanceof Node node) {
      nodeList.remove(node);
    } else if (object instanceof GroupNode groupNode) {
      groupNodeList.remove(groupNode);
    }
    object.setGroupNode(null);
  }

  public void removeAll(Stream<AbstractCanvasObject> stream) {
    stream.forEach(this::remove);
  }

  public void removeAll(Collection<AbstractCanvasObject> collection) {
    collection.forEach(this::remove);
  }

  public Stream<AbstractCanvasObject> getObjectsInThisLayer() {
    return Stream.of(objectList.stream(), switchList.stream(), nodeList.stream(),
            groupNodeList.stream())
        .flatMap(s -> s);
  }

  public Stream<AbstractCanvasObject> getAllObjectsRecursive() {
    Stream<AbstractCanvasObject> objects = Stream.of(objectList.stream(), switchList.stream(),
            nodeList.stream())
        .flatMap(s -> s);
    return Stream.concat(objects,
        groupNodeList.stream().flatMap(GroupNode::getObjectsInThisLayer));

  }

  public Stream<HolonObject> getAllHolonObjectsRecursive() {
    return Stream.concat(objectList.stream(),
        groupNodeList.stream().flatMap(GroupNode::getAllHolonObjectsRecursive));
  }

  public Stream<HolonSwitch> getAllSwitchObjectsRecursive() {
    return Stream.concat(switchList.stream(),
        groupNodeList.stream().flatMap(GroupNode::getAllSwitchObjectsRecursive));
  }

  public Stream<Node> getAllNodeObjectsRecursive() {
    return Stream.concat(nodeList.stream(),
        groupNodeList.stream().flatMap(GroupNode::getAllNodeObjectsRecursive));
  }

  public Stream<GroupNode> getAllGroupNodeObjectsRecursive() {
    return Stream.concat(groupNodeList.stream(),
        groupNodeList.stream().flatMap(GroupNode::getAllGroupNodeObjectsRecursive));
  }

  public Stream<HolonObject> getHolonObjects() {
    return objectList.stream();
  }

  public Stream<HolonSwitch> getSwitches() {
    return switchList.stream();
  }

  public Stream<Node> getNodes() {
    return nodeList.stream();
  }

  public Stream<GroupNode> getGroupNodes() {
    return groupNodeList.stream();
  }

  public Stream<HolonElement> getHolonElements() {
    return objectList.stream().flatMap(HolonObject::elementsStream);
  }

  public Stream<HolonElement> getAllHolonElements() {
    return Stream.concat(getHolonElements(),
        groupNodeList.stream().flatMap(GroupNode::getAllHolonElements));
  }

  public Stream<Flexibility> getAllFlexibilities() {
    return getAllHolonObjectsRecursive().flatMap(
        hO -> hO.elementsStream().flatMap(ele -> ele.flexList.stream()));
  }

  public void ungroup() {
    getObjectsInThisLayer().forEach(obj -> {
      obj.setGroupNode(null);
    });
    getGroupNode().ifPresent(parent -> {
      parent.addAll(getObjectsInThisLayer());
      parent.remove(this);
    });

    clear();
  }

  public float getTotalConsumption() {
    return getAllHolonObjectsRecursive().map(HolonObject::getConsumption).reduce(0.f, Float::sum);
  }

  public float getTotalProduction() {
    return getAllHolonObjectsRecursive().map(HolonObject::getProduction).reduce(0.f, Float::sum);
  }


  public void clear() {
    objectList.clear();
    switchList.clear();
    nodeList.clear();
    groupNodeList.clear();
  }

  @Override
  public void postDeserialize() {
    getObjectsInThisLayer().forEach(obj -> obj.setGroupNode(this));
  }


  @Override
  public String toString() {
    return "" + objectList + switchList + nodeList + groupNodeList;
  }
}
