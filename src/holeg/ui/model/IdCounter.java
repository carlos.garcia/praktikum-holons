package holeg.ui.model;

/**
 * Static counter ID-Counter for all Cps Objects.
 *
 * @author Gruppe14
 */
public class IdCounter {

  private static int counter = 1;

  /**
   * Return the next ID and increment the ID counter by 1.
   *
   * @return the next ID
   */
  public static synchronized int next() {
    return counter++;
  }

  /**
   * Return the Counter.
   *
   * @return the counter
   */
  public static int get() {
    return counter;
  }

  /**
   * Set the Counter.
   *
   * @param counter the counter to set
   */
  public static void set(int counter) {
    IdCounter.counter = counter;
  }

  /**
   * Reset the Counter.
   */
  public static void reset() {
    counter = 0;
  }
}
