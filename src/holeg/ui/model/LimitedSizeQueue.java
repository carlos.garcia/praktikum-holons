package holeg.ui.model;

import java.util.ArrayList;

public class LimitedSizeQueue<T> extends ArrayList<T> {

  private final int maxSize;

  public LimitedSizeQueue(int size) {
    this.maxSize = size;
  }

  public boolean add(T k) {
    boolean r = super.add(k);
    if (size() > maxSize) {
      removeRange(0, size() - maxSize);
    }
    return r;
  }

  public void removeRange(int a, int b) {
    super.removeRange(a, b);
  }

  public T getYoungest() {
    return get(size() - 1);
  }

  public T getOldest() {
    return get(0);
  }
}
