package holeg.ui.model;

import holeg.model.AbstractCanvasObject;
import holeg.model.Edge;
import holeg.ui.view.category.Category;
import holeg.utility.math.vector.Vec2i;
import java.io.File;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class GuiSettings {

  private static final Set<Edge> selectedEdges = new HashSet<>();
  private static final Set<Category> categories = new HashSet<>();
  private static final Set<AbstractCanvasObject> clipboardObjects = new HashSet<>();
  private static final Set<Edge> clipboardEdges = new HashSet<>();
  private static final Set<AbstractCanvasObject> selectedObjects = new HashSet<>();
  public static Vec2i canvasSize = new Vec2i(3000, 3000);
  public static int timerSpeed = 1000;
  public static float maxCapacityForNewCreatedEdges = 10000;
  public static float dragThresholdDistance = 5;
  private static int pictureScale = 50; // Picture Scale
  private static int halfPictureScale = pictureScale / 2;
  private static File actualSaveFile = null;

  public static Optional<File> getActualSaveFile() {
    return Optional.ofNullable(actualSaveFile);
  }

  public static void setActualSaveFile(File file) {
    actualSaveFile = file;
  }


  public static int getPictureScale() {
    return pictureScale;
  }

  public static void setPictureScale(int value) {
    pictureScale = value;
    halfPictureScale = (value + 1) / 2;
  }

  public static int getPictureScaleDiv2() {
    return halfPictureScale;
  }

  public static Set<Edge> getSelectedEdges() {
    return selectedEdges;
  }

  public static Set<Category> getCategories() {
    return categories;
  }

  public static Set<AbstractCanvasObject> getClipboardObjects() {
    return clipboardObjects;
  }

  public static Set<Edge> getClipboardEdges() {
    return clipboardEdges;
  }

  public static Set<AbstractCanvasObject> getSelectedObjects() {
    return selectedObjects;
  }
}
