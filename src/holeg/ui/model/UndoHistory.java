package holeg.ui.model;

import java.util.Optional;

public class UndoHistory {

  public static final int NumberOfSaves = 35;
  private static final LimitedSizeQueue<String> jsonSaves = new LimitedSizeQueue<>(NumberOfSaves);
  private static int actualIndex = -1;


  public static boolean canUndo() {
    return actualIndex > 0;
  }

  public static Optional<String> undo() {
    if (canUndo()) {
      actualIndex -= 1;
      return Optional.of(jsonSaves.get(actualIndex));
    }
    return Optional.empty();
  }

  public static boolean canRedo() {
    return actualIndex < jsonSaves.size() - 1;
  }

  public static Optional<String> redo() {
    if (canRedo()) {
      actualIndex += 1;
      return Optional.of(jsonSaves.get(actualIndex));
    }
    return Optional.empty();
  }

  public static void addSave(String save) {
    if (canRedo()) {
      jsonSaves.removeRange(actualIndex + 1, jsonSaves.size());
    }
    if (actualIndex < NumberOfSaves) {
      actualIndex += 1;
    }
    jsonSaves.add(save);
  }

}