package holeg.ui.model;

import holeg.model.HolonElement;
import jdk.jfr.Unsigned;

public class PriorityCount {

  @Unsigned
  public int low, medium, high, essential;

  public void add(PriorityCount other) {
    low += other.low;
    medium += other.medium;
    high += other.high;
    essential += other.essential;
  }

  public void count(HolonElement element) {
    switch (element.getPriority()) {
      case Essential:
        essential++;
        break;
      case High:
        high++;
        break;
      case Medium:
        medium++;
        break;
      case Low:
        low++;
        break;
      default:
        break;

    }
  }
}
