package holeg.ui.view.dialog;

import holeg.model.Edge;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.model.GuiSettings;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * Popup for Editing Edges.
 */
public class EditEdgesPopUp extends JDialog {

  private final JTextField capacityField;
  private final JRadioButton existingEdgesRadioButton = new JRadioButton(
      "Change for all existing Edges only");
  private final JRadioButton newEdgesRadioButton = new JRadioButton(
      "Change for new created Edges only");
  private final JRadioButton existingAndNewEdgesRadioButton = new JRadioButton(
      "Change for all existing and new created Edges");
  private final Control control;
  private float capacity;


  /**
   * Constructor.
   */
  public EditEdgesPopUp(JFrame parentFrame, Control control) {
    super((java.awt.Frame) null, true);
    setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
    this.setTitle("Edit Capacities of Edges");
    setBounds(100, 100, 400, 220);
    setLocationRelativeTo(parentFrame);
    getContentPane().setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(null);
    this.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    JLabel lblMaximumCapacity = new JLabel("Maximum Capacity:");
    lblMaximumCapacity.setFont(new Font("Tahoma", Font.PLAIN, 11));
    lblMaximumCapacity.setBounds(10, 11, 98, 14);
    contentPanel.add(lblMaximumCapacity);

    capacityField = new JTextField();
    capacityField.setBounds(107, 8, 120, 20);
    contentPanel.add(capacityField);
    capacityField.setColumns(10);

    existingEdgesRadioButton.setBounds(10, 39, 330, 23);
    contentPanel.add(existingEdgesRadioButton);

    newEdgesRadioButton.setBounds(10, 65, 330, 23);
    contentPanel.add(newEdgesRadioButton);

    existingAndNewEdgesRadioButton.setBounds(10, 95, 400, 23);
    contentPanel.add(existingAndNewEdgesRadioButton);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.setActionCommand("Cancel");
    btnCancel.addActionListener(clicked -> dispose());

    btnCancel.setBounds(285, 147, 89, 23);
    contentPanel.add(btnCancel);

    JButton btnOk1 = new JButton("OK");
    btnOk1.addActionListener(clicked -> {
      try {
        capacity = Float.parseFloat(capacityField.getText());
        if (capacity < 0) {
          throw new NumberFormatException();
        }
        if (existingEdgesRadioButton.isSelected()) {
          changeForExisting(capacity);
          dispose();
        } else if (newEdgesRadioButton.isSelected()) {
          changeForNew(capacity);
          dispose();
        } else if (existingAndNewEdgesRadioButton.isSelected()) {
          changeForExAndNew(capacity);
          dispose();
        } else {
          JOptionPane.showMessageDialog(new JFrame(), "Please select one of the options");
        }
      } catch (NumberFormatException eex) {
        JOptionPane.showMessageDialog(new JFrame(),
            "Please enter a number greater or equal 0 in the Field for Maximum Capacity");
      }
    });
    btnOk1.setBounds(186, 147, 89, 23);
    contentPanel.add(btnOk1);
    this.setTitle("Edit Edge Capacities");
    ButtonGroup bG = new ButtonGroup();
    bG.add(existingAndNewEdgesRadioButton);
    bG.add(newEdgesRadioButton);
    bG.add(existingEdgesRadioButton);
    existingAndNewEdgesRadioButton.setSelected(true);
    this.control = control;
    this.setVisible(true);
  }

  /**
   * set edge capacity for new edges.
   *
   * @param cap the capacity
   */
  public void changeForNew(float cap) {
    GuiSettings.maxCapacityForNewCreatedEdges = cap;
    control.resetSimulation();
  }

  /**
   * Set Capacity for all existing Edges.
   *
   * @param cap the Capacity
   */
  public void changeForExisting(float cap) {
    /*
     * for(SubNet sn: controller.getSimManager().getSubNets()){ for(CpsEdge
     * edge: sn.getEdges()){ edge.setCapacity(cap); } } for(CpsEdge edge:
     * controller.getSimManager().getBrokenEdges()){ edge.setCapacity(cap);
     * }
     */
    for (Edge edge : control.getModel().getEdgesOnCanvas()) {
      edge.maxCapacity = cap;
    }
    control.resetSimulation();
    control.updateStateForCurrentIteration();
  }


  /**
   * Set the Capacity for all existing and new edges.
   *
   * @param cap the capacity
   */
  public void changeForExAndNew(float cap) {
    changeForNew(cap);
    changeForExisting(cap);
  }

}
