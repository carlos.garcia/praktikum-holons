package holeg.ui.view.dialog;

import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.model.GuiSettings;
import holeg.ui.view.canvas.Canvas;
import holeg.ui.view.image.Import;
import holeg.ui.view.main.Gui;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A Dialog for changing the canvas size.
 */
public class CanvasResizePopUp extends JDialog {

  private final JTextField widthTextField = new JTextField();
  private final JTextField heightTextField = new JTextField();
  Control controller;

  public CanvasResizePopUp(Control controller, Gui gui) {
    super(gui, true);
    this.controller = controller;
    // properties and stuff
    this.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    this.setTitle("Set the Size of the View");
    setBounds(200, 100, 200, 100);
    setLocationRelativeTo(gui);

    // MainPanel
    widthTextField.setText("" + GuiSettings.canvasSize.getX());
    heightTextField.setText("" + GuiSettings.canvasSize.getY());
    JPanel mainPanel = new JPanel();
    JLabel lblWidth = new JLabel("Width:");
    mainPanel.add(lblWidth);
    mainPanel.add(widthTextField);
    JLabel lblHeight = new JLabel("Height:");
    mainPanel.add(lblHeight);
    mainPanel.add(heightTextField);
    mainPanel.setBackground(Color.WHITE);

    // Button Panel
    JButton btnOk = new JButton("OK");
    btnOk.addActionListener(e -> {
      GuiSettings.canvasSize.setX(Integer.parseInt(widthTextField.getText()));
      GuiSettings.canvasSize.setY(Integer.parseInt(heightTextField.getText()));
      gui.canvasCollection.resetCanvasSizes();
      controller.getModel().getCanvas().getAllObjectsRecursive().forEach(obj -> {
        obj.setPosition(Canvas.boundsToCanvas(obj.getPosition()));
      });
      controller.OnCanvasUpdate.broadcast();
      dispose();
    });
    JButton btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(clicked -> dispose());
    JPanel buttonPanel = new JPanel();
    buttonPanel.add(btnOk);
    buttonPanel.add(btnCancel);
    buttonPanel.setBackground(Color.WHITE);

    // Add to ContentPane
    getContentPane().add(mainPanel, BorderLayout.CENTER);
    getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    this.setVisible(true);
  }

}
