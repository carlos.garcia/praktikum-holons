package holeg.ui.view.dialog;

import holeg.model.HolonElement;
import holeg.model.HolonObject;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.model.GuiSettings;
import holeg.ui.view.category.Category;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import java.awt.Choice;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * PopUp for creating Holon Object Template.
 */
public class CreateTemplatePopUp extends JDialog {

  /**
   * Template HolonObject
   */
  private final HolonObject template;

  /**
   * HolonElementList
   */
  private final DefaultListModel<String> listModel;
  private final List<HolonElement> holonElementList = new ArrayList<>();

  /**
   * HolonElement List
   */
  private final JList<String> list;

  // Template Attributes

  // PopUp Parts
  private final Control controller;
  /**
   * name textfield
   */
  private final JTextField textField_name;
  /**
   * textField for path
   */
  private final JTextField textField_imagePath;
  /**
   * Category Selection
   */
  Choice choice;
  /**
   * Image Preview
   */
  JLabel lblImagePreview;

  /**
   * parent Frame
   */
  JFrame parent;

  /**
   * Create the dialog.
   * <p>
   * true if edit
   *
   * @param obj the object
   */
  public CreateTemplatePopUp(HolonObject obj,
      JFrame parentFrame, Control controller) {
    setResizable(false);
    /*
     * use Category Controller an stuff lul
     */

    /*
     * initialize Data
     */
    template = new HolonObject(obj);
    this.parent = parentFrame;
    this.controller = controller;
    /*
     * create Frame and GUI
     */
    setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    setBounds(100, 100, 476, 344);
    setLocationRelativeTo(parentFrame);
    getContentPane().setLayout(new BorderLayout());

    JPanel contentPanel = new JPanel();
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(null);

    JLabel lblCategory = new JLabel("Category:");
    lblCategory.setBounds(12, 13, 68, 22);
    contentPanel.add(lblCategory);

    choice = new Choice();
    choice.setBounds(86, 13, 172, 22);
    contentPanel.add(choice);
    // add categories
    if (GuiSettings.getCategories().isEmpty()) {
      this.controller.createCategoryWithName("Template");
    }
    // add Categories to the choice
    for (Category c : GuiSettings.getCategories()) {
      choice.add(c.getName());
    }

    JLabel lblName = new JLabel("Name:");
    lblName.setBounds(12, 48, 56, 16);
    contentPanel.add(lblName);

    textField_name = new JTextField();
    textField_name.setBounds(86, 48, 172, 22);
    contentPanel.add(textField_name);
    textField_name.setColumns(10);
    textField_name.setText(template.getName());

    JLabel lblImage = new JLabel("Image:");
    lblImage.setBounds(12, 89, 56, 16);
    contentPanel.add(lblImage);

    textField_imagePath = new JTextField();
    textField_imagePath.setBounds(86, 86, 172, 22);
    contentPanel.add(textField_imagePath);
    textField_imagePath.setColumns(10);
    textField_imagePath.setText(template.getImagePath());

    JButton btnBrowseImage = new JButton("BrowseImage");
    btnBrowseImage.setBounds(268, 85, 117, 25);
    contentPanel.add(btnBrowseImage);
    btnBrowseImage.addActionListener(clicked -> fileChooser());

    lblImagePreview = new JLabel("Image Preview");
    lblImagePreview.setIcon(new ImageIcon(Import.loadImage(
        template.getImagePath(), 62, 62)));
    lblImagePreview.setBounds(298, 13, 62, 62);
    contentPanel.add(lblImagePreview);

    listModel = new DefaultListModel<>();

    template.elementsStream().forEach(hE -> {
      listModel.addElement(hE.getName()
          + ": " + hE.getEnergy() + "U");
      holonElementList.add(hE);
    });
    template.clearElements();
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setBounds(22, 118, 236, 150);
    contentPanel.add(scrollPane);
    list = new JList<>(listModel);
    scrollPane.setViewportView(list);

    JButton btnDeleteElement = new JButton("Delete Element");
    btnDeleteElement.setBounds(268, 228, 140, 25);
    contentPanel.add(btnDeleteElement);
    btnDeleteElement.addActionListener(e -> removeElement());

    JButton btnEditElement = new JButton("Edit Element");
    btnEditElement.setBounds(268, 190, 140, 25);
    contentPanel.add(btnEditElement);
    btnEditElement.addActionListener(e -> editElement());

    JButton btnAddElement = new JButton("Add Element");
    btnAddElement.setBounds(268, 152, 140, 25);
    contentPanel.add(btnAddElement);
    btnAddElement.addActionListener(e -> addElement());

    JButton btnCancel = new JButton("Cancel");
    btnCancel.setBounds(384, 277, 74, 25);
    contentPanel.add(btnCancel);
    btnCancel.addActionListener(e -> dispose());

    /*
     * Add Template Button
     */
    JButton btnAddTemplate = new JButton("Add Template");
    btnAddTemplate.setBounds(75, 271, 113, 25);
    contentPanel.add(btnAddTemplate);
    btnAddTemplate.addActionListener(e -> createTemplate());

    /*
     * Title
     */
    setTitle("Create Template Menu");
    this.setVisible(true);
  }

  /**
   * Choose the file.
   */
  private void fileChooser() {
    JFileChooser fileChooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter(
        "png, jpg or jpeg", "png", "jpg", "jpeg");
    fileChooser.setFileFilter(filter);
    int returnValue = fileChooser.showOpenDialog(null);
    if (returnValue == JFileChooser.APPROVE_OPTION) {
      File selectedFile = fileChooser.getSelectedFile();
      String filePath = selectedFile.getAbsolutePath();
      textField_imagePath.setText(filePath);
      ImageIcon icon = new ImageIcon(Import.loadImage(filePath, 62,
          62));
      lblImagePreview.setIcon(icon);
    } else {
      System.out.println("Failed to Load");
    }

  }

  /**
   * create the template and add it to the category
   */
  private void createTemplate() {
    template.setName(textField_name.getText());
    template.setImagePath(textField_imagePath.getText());
    template.clearElements();
    template.add(holonElementList);
    controller.findCategoryWithName(choice
            .getItem(choice.getSelectedIndex()))
        .ifPresent(cat -> controller.addObject(cat, template.getName(),
            template.elementsStream().toList(), template.getImagePath()));
    this.dispose();
  }

  /**
   * Add an Holon Element to the template
   */
  private void addElement() {
    AddElementPopUp popUp = new AddElementPopUp(parent);
    popUp.setActualHolonObject(template);
    popUp.setVisible(true);
    HolonElement he = popUp.getElement();
    if (he != null) {
      listModel.addElement(he.getName()
          + ": " + he.getEnergy() + "U");
      holonElementList.add(he);
    }
  }

  /**
   * Removes the Selected Element from the template
   */
  private void removeElement() {
    int index = list.getSelectedIndex();
    if (index == -1) {
      return;
    }
    listModel.remove(index);
    holonElementList.remove(index);
  }

  /**
   * Edits the selected HolonElement
   */
  private void editElement() {
    int index = list.getSelectedIndex();
    if (index == -1) {
      return;
    }

    AddElementPopUp popUp = new AddElementPopUp(parent);
    popUp.setActualHolonObject(template);
    popUp.setElement(template.elementsStream().toList().get(index));
    popUp.setVisible(true);
    HolonElement he = popUp.getElement();
    if (he != null) {
      listModel.remove(index);
      holonElementList.remove(index);
      listModel.addElement(he.getName()
          + ": " + he.getEnergy() + "U");
      holonElementList.add(he);
    }
  }

}
