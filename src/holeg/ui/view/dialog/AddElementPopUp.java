package holeg.ui.view.dialog;

import holeg.model.HolonElement;
import holeg.model.HolonObject;
import holeg.preferences.ColorPreference;
import holeg.preferences.ImagePreference;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * popup for adding an Holon Element to a holon Object.
 */
public class AddElementPopUp extends JDialog {

  /* Data */
  /**
   * Panel containing everything
   */
  private final JPanel contentPanel = new JPanel();
  /**
   * Textfield for entering a Name for the Element
   */
  private final JTextField elementName;

  /* GUI */
  /**
   * Textfield for the energy the Element consumes/produces
   */
  private final JTextField providedEnergy;
  /**
   * Element is active if checked
   */
  JCheckBox checkBoxActive;
  /**
   * Holon Object the Element should be added to
   */
  private HolonObject tempCps;
  /**
   * Holon Element that should be edited (if in edit Modus
   */
  private HolonElement hl;

  AddElementPopUp(JFrame parentFrame) {
    super((java.awt.Frame) null, true);
    this.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
    setBounds(100, 100, 400, 245);
    setLocationRelativeTo(parentFrame);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(null);
    this.setTitle("Add Element to Object");


    /* Element Name Textfield and Label */
    elementName = new JTextField();
    elementName.addKeyListener(new KeyListener() {
      @Override
      public void keyPressed(KeyEvent arg0) {
      }

      @Override
      public void keyReleased(KeyEvent e) {
      }

      @Override
      public void keyTyped(KeyEvent e) {
        elementName.setBackground(Color.WHITE);
      }
    });

    JLabel lblElementName = new JLabel("Element Name:");
    lblElementName.setBounds(10, 10, 100, 20);
    contentPanel.add(lblElementName);
    elementName.setBounds(130, 10, 110, 20);
    contentPanel.add(elementName);
    elementName.setColumns(10);

    /* Add Provided Energy Label and Textfield */
    JLabel lblProvidedEnergy = new JLabel("Provided Energy:");
    lblProvidedEnergy.setBounds(10, 50, 120, 20);
    contentPanel.add(lblProvidedEnergy);

    providedEnergy = new JTextField();
    providedEnergy.setBounds(130, 50, 110, 20);
    contentPanel.add(providedEnergy);
    providedEnergy.setColumns(10);
    providedEnergy.setText("0");

    checkBoxActive = new JCheckBox("Active");
    checkBoxActive.setSelected(true);
    checkBoxActive.setBounds(250, 50, 115, 20);
    contentPanel.add(checkBoxActive);


    /* Add Buttons and Actions */
    JPanel buttonPane = new JPanel();
    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
    getContentPane().add(buttonPane, BorderLayout.SOUTH);

    JButton okButton = new JButton("OK");
    okButton.addActionListener(arg0 -> okAction());
    okButton.setActionCommand("OK");
    buttonPane.add(okButton);
    getRootPane().setDefaultButton(okButton);

    JButton cancelButton = new JButton("Cancel");
    cancelButton.setActionCommand("Cancel");
    buttonPane.add(cancelButton);
    cancelButton.addActionListener(e -> dispose());
  }

  /**
   * Sets the actual Cps.
   *
   * @param cps actual Cps
   */
  void setActualHolonObject(HolonObject cps) {
    this.tempCps = cps;
  }

  /**
   * Returns the created Element.
   *
   * @return the Element
   */
  public HolonElement getElement() {
    return hl;
  }

  public void setElement(HolonElement holonElement) {
    hl = holonElement;
    elementName.setText(hl.getName());
    providedEnergy.setText("" + hl.getEnergy());
    checkBoxActive.setSelected(hl.active);

  }

  /**
   * Trys to create/edit the Element
   */
  private void okAction() {
    boolean repeated = false;
    for (HolonElement e : tempCps.elementsStream().toList()) {
      if (elementName.getText().equals(e.getName()) && (hl == null)) {
        repeated = true;
        break;
      }
    }
    if (elementName.getText().length() != 0 && !repeated) {
      try {
        float energy = Float.parseFloat(providedEnergy.getText());
        if (hl == null) {
          hl = new HolonElement(this.tempCps, elementName.getText(), energy);
        } else {
          hl.setName(elementName.getText());
          hl.setEnergy(energy);
          hl.active = checkBoxActive.isSelected();
        }
        dispose();
      } catch (NumberFormatException e) {
        JOptionPane.showMessageDialog(new JFrame(),
            "Please enter numbers in the Fields amount and Energy");
      }
    } else {
      if (elementName.getText().isEmpty()) {
        JLabel errorString = new JLabel("No name");
        errorString.setBounds(240, 8, 100, 20);
        contentPanel.add(errorString);
      } else if (repeated) {
        JLabel errorString = new JLabel("Name already given");
        errorString.setBounds(250, 8, 100, 20);
        contentPanel.add(errorString);
      }
      elementName.setBackground(ColorPreference.Dialog.BackgroundColor);
    }
  }
}
