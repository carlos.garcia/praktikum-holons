package holeg.ui.view.dialog;

import holeg.model.AbstractCanvasObject;
import holeg.model.HolonElement;
import holeg.model.HolonObject;
import holeg.preferences.ColorPreference;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.view.category.Category;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Popup for adding a Holon Object to a Category.
 */
public class AddObjectPopUp extends JDialog {

  private static final Logger log = Logger.getLogger(AddObjectPopUp.class.getName());
  private final JTextField objectName;
  private final JTextField sourcePath;
  private final ArrayList<HolonElement> hElements;
  private final DefaultListModel<String> listModel;
  private final JList<String> list;
  // private HolonObject theObject;
  private final Control control;
  private final JLabel lblImagePreview;
  private final AbstractCanvasObject toEdit;
  private AddElementPopUp addElement;
  private String imagePath;
  private File selectedFile = null;
  private String filePath = " ";
  private boolean imageChanged = false;

  /**
   * Create the dialog.
   *
   * @param obj      the object
   * @param category the categorie
   */
  public AddObjectPopUp(Control control, HolonObject obj, Category category, JFrame parentFrame) {
    this.control = control;
    toEdit = obj;
    this.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    setBounds(100, 100, 450, 342);
    setLocationRelativeTo(parentFrame);
    getContentPane().setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(null);
    hElements = new ArrayList<>();
    this.setTitle("Add Object Menu");
    {
      JLabel lblName = new JLabel("Name:");
      lblName.setHorizontalAlignment(SwingConstants.CENTER);
      lblName.setBounds(28, 21, 76, 14);
      contentPanel.add(lblName);
    }
    {
      objectName = new JTextField();
      objectName.addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent arg0) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

        }

        @Override
        public void keyTyped(KeyEvent e) {
          objectName.setBackground(Color.WHITE);
        }
      });
      objectName.setText(obj.getName());
      objectName.setBounds(98, 18, 172, 20);
      contentPanel.add(objectName);
      objectName.setColumns(10);
    }
    {
      JButton btnBrowseImage = new JButton("Browse Image");
      btnBrowseImage.setBounds(10, 75, 134, 23);
      contentPanel.add(btnBrowseImage);
      btnBrowseImage.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
          fileChooser();
        }
      });

    }
    {
      lblImagePreview = new JLabel("");
      lblImagePreview.setBounds(295, 3, 50, 50);
      contentPanel.add(lblImagePreview);
    }
    {
      sourcePath = new JTextField();
      sourcePath.addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent arg0) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void keyTyped(KeyEvent e) {
          sourcePath.setBackground(Color.WHITE);
        }
      });
      if (!obj.getImagePath().equals("")) {
        log.info("IMAGEPATH:" + obj.getImagePath());
        lblImagePreview.setIcon(new ImageIcon(Import.loadImage(obj.getImagePath(), 50, 50)));
        this.filePath = obj.getImagePath();
        sourcePath.setText(filePath);
      }
      sourcePath.setBounds(148, 77, 271, 20);
      contentPanel.add(sourcePath);

      sourcePath.setColumns(10);
    }
    {
      JButton btnAddDefaultElement = new JButton("Add Element");
      btnAddDefaultElement.addActionListener(actionEvent -> {
        addElement = new AddElementPopUp(parentFrame);
        addElement.setActualHolonObject((HolonObject) toEdit);
        addElement.setVisible(true);
        HolonElement hl = addElement.getElement();
        // if (hl != null) {
        // hl.setSav(givenCategory);
        // }
        // hl.setObj(objectName.getText());
        addElement(hl);
      });

      btnAddDefaultElement.setBounds(270, 144, 142, 23);
      contentPanel.add(btnAddDefaultElement);
    }
    {
      JScrollPane scrollPane = new JScrollPane();
      scrollPane.setBounds(10, 114, 236, 150);
      contentPanel.add(scrollPane);
      {

        listModel = new DefaultListModel<>();

        /*
         * HolonElement hel = new HolonElement("Test", 100, 5); String name =
         * hel.getEleName(); for (int i = 0; i < 11; i++) { hel.setEleName(name + i);
         * addElement(hel); }
         */
        list = new JList<>(listModel);
        scrollPane.setViewportView(list);
      }
    }
    obj.elementsStream().forEach(this::addElement);
    {
      JButton btnNewButton = new JButton("Delete Element");
      btnNewButton.addActionListener(actionEvent -> {
        int selectedIndex = list.getSelectedIndex();
        if (selectedIndex != -1) {
          listModel.remove(selectedIndex);
          hElements.remove(selectedIndex);
        }
      });
      btnNewButton.setBounds(270, 182, 142, 27);
      contentPanel.add(btnNewButton);
    }

    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.addMouseListener(new MouseAdapter() {
          public void mouseClicked(MouseEvent e) {
//						Component frame = null;
            if (objectName.getText().length() > 0) {
              if (sourcePath.getText().equals(filePath)) {
                imagePath = filePath;
                if (imageChanged) {
                  copieFile();
                }
                imageChanged = false;
                if (category != null) {
                  category.getObjects().remove(toEdit);
                  AddObjectPopUp.this.control.addObject(category, objectName.getText(), hElements,
                      imagePath);
                }
                dispose();
              } else {
                sourcePath.setBackground(ColorPreference.Dialog.BackgroundColor);
              }
            } else {
              objectName.setBackground(ColorPreference.Dialog.BackgroundColor);

            }
          }
        });
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
        cancelButton.addActionListener(e -> dispose());
      }
      setVisible(true);
    }
  }

  /**
   * adds a Holon Element.
   *
   * @param hl the HolonElement
   */
  private void addElement(HolonElement hl) {
    hElements.add(hl);
    listModel.addElement("x: " + hl.getName() + " " + hl.getEnergy() + "U");
  }

  /**
   * Choose the file.
   */
  private void fileChooser() {
    JFileChooser fileChooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("png, jpg or jpeg", "png", "jpg",
        "jpeg");
    fileChooser.setFileFilter(filter);
    int returnValue = fileChooser.showOpenDialog(null);
    if (returnValue == JFileChooser.APPROVE_OPTION) {
      selectedFile = fileChooser.getSelectedFile();
      filePath = selectedFile.getAbsolutePath();
      sourcePath.setText(filePath);
      ImageIcon icon = new ImageIcon(Import.loadImage(filePath, 50, 50));
      lblImagePreview.setIcon(icon);
      imageChanged = true;
    } else {
      System.out.println("Failed to Load");
    }

  }

  /**
   * Copies the File.
   */
  private void copieFile() {
    InputStream inStream;
    OutputStream outStream;
    try {
      File source = new File(filePath);
      //TODO: "CopieFile"
      File dest = new File(System.getProperty("user.home") + "/.config/HolonGUI/Images/");
      Files.createDirectories(dest.toPath());
      dest = new File(dest, selectedFile.getName());
      imagePath = "" + dest;

      inStream = new FileInputStream(source);
      outStream = new FileOutputStream(dest);
      byte[] buffer = new byte[1024];

      int length;
      while ((length = inStream.read(buffer)) > 0) {
        outStream.write(buffer, 0, length);
      }

      inStream.close();
      outStream.close();
    } catch (IOException eex) {
      eex.printStackTrace();
    }
  }

}