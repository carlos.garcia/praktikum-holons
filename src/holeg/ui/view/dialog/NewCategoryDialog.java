package holeg.ui.view.dialog;

import holeg.ui.controller.Control;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A simple Dialog for creating new Categories.
 */
public class NewCategoryDialog extends JDialog {

  private final JPanel contentPanel = new JPanel(new BorderLayout());
  private final JPanel mainPanel = new JPanel();
  private final JPanel buttonPanel = new JPanel();
  private final JLabel categoryLabel = new JLabel("Category:");
  private final JTextField nameTextField = new JTextField("Name");
  private final JButton okayButton = new JButton("Okay");
  private final JButton cancelButton = new JButton("Cancel");
  private final Control control;

  public NewCategoryDialog(Control control, JFrame parentFrame) {
    super(parentFrame);
    this.control = control;
    init();
    this.setVisible(true);
    this.pack();
    setLocationRelativeTo(parentFrame);
  }

  private void init() {
    this.setTitle("Create new category");
    initLayout();
    initButtons();
  }

  private void initLayout() {
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    contentPanel.setPreferredSize(new Dimension(200, 60));
    mainPanel.setLayout(new GridLayout(1, 2));
    mainPanel.add(categoryLabel);
    mainPanel.add(nameTextField);
    okayButton.setDefaultCapable(true);
    buttonPanel.add(okayButton);
    buttonPanel.add(cancelButton);
    contentPanel.add(mainPanel);
    contentPanel.add(buttonPanel, BorderLayout.PAGE_END);

    this.setContentPane(contentPanel);
  }

  private void initButtons() {
    okayButton.addActionListener(clicked -> {
      control.createCategoryWithName(nameTextField.getText());
      dispose();
    });
    cancelButton.addActionListener(clicked -> dispose());
  }
}
