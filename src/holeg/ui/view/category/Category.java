package holeg.ui.view.category;

import holeg.model.AbstractCanvasObject;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Class "Category" performs the functionality of listing elements into groups. Each Category
 * contains an ArrayList of CpsObjects, a name and a HashMap of ObjIdx.
 *
 * @author Gruppe14
 */

public class Category {

  // objects: is a ArrayList of all Objects that belongs to the Category
  private Set<AbstractCanvasObject> objects = new HashSet<AbstractCanvasObject>();
  // name: is a String chosen by the User
  private String name;

  /**
   * Category Constructor.
   *
   * @param name name of the Category
   */
  public Category(String name) {
    setName(name);
  }

  /**
   * Getter for all CpsObjects.
   *
   * @return the objects
   */
  public Set<AbstractCanvasObject> getObjects() {
    return objects;
  }

  /**
   * Set a new ArrayList of CpsObjects.
   *
   * @param objects the objects to set
   */
  public void setObjects(Set<AbstractCanvasObject> objects) {
    this.objects = objects;
  }

  /**
   * Getter the name of the Category.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Set the name of the Category to a new one.
   *
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }


  public Optional<AbstractCanvasObject> findObjectWithName(String name) {
    return objects.stream().filter(obj -> obj.getName().equals(name)).findFirst();
  }

  public boolean removeObjectsWithName(String name) {
    return objects.removeIf(obj -> obj.getName().equals(name));
  }
}
