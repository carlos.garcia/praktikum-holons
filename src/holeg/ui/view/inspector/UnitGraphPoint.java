package holeg.ui.view.inspector;

import holeg.utility.math.vector.Vec2f;
import holeg.utility.math.vector.Vec2i;

/**
 * A class for saving all points of a Object with a editable state. e.g HolonElement, HolonSwitch
 */
public class UnitGraphPoint {

  /**
   * Relative position in the UnitGraphCanvas
   */
  public double x, y;
  /**
   * To determine if this point has changed to only write back points that are changed.
   */
  public boolean changed;
  /**
   * The displayed Position of the UnitGraphPoint
   */
  public Vec2i displayedPosition;

  /**
   * Default Constructor
   */
  public UnitGraphPoint(double x, double y, boolean changed) {
    this.x = x;
    this.y = y;
    this.changed = changed;
  }

  /**
   * Constructor with a Point2D.Double
   */
  public UnitGraphPoint(Vec2f pos, boolean changed) {
    this.x = pos.getX();
    this.y = pos.getY();
    this.changed = changed;
  }

  /**
   * Constructor with a Point2D.Double when changed not specified is false;
   */
  public UnitGraphPoint(Vec2f pos) {
    this.x = pos.getX();
    this.y = pos.getY();
    this.changed = false;
  }

  /**
   * Calculate the corresponding position in pixels [0,width] from the relative position [0.0,1.0].
   * @param border the border size
   * @param widthWithBorder the width
   * @param heightWithBorder the height
   */
  public void calcDisplayedPosition(int border, int widthWithBorder, int heightWithBorder) {
    //Relativ to Border
    //1-p.y because its on the Top
    displayedPosition = new Vec2i((int) (x * widthWithBorder) + border,
        (int) ((1 - y) * heightWithBorder) + border);
  }

  public Vec2f getPoint() {
    return new Vec2f((float) x, (float) y);
  }

  @Override
  public String toString() {
    return "[" + x + ":" + y + "]";
  }
}
