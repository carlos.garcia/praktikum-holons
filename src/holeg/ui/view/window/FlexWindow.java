package holeg.ui.view.window;

import holeg.model.Constrain;
import holeg.model.Flexibility;
import holeg.model.Flexibility.FlexState;
import holeg.model.GroupNode;
import holeg.model.HolonElement;
import holeg.model.HolonElement.Priority;
import holeg.model.HolonObject;
import holeg.preferences.ColorPreference;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.view.image.Import;
import holeg.utility.listener.WindowClosingListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

/**
 * An overview for flexibilities in holeg, with basic manipulation of flexibilities.
 */
public class FlexWindow extends JFrame {

  private final static int boxSideLength = 70;
  private final JTabbedPane contentPanel = new JTabbedPane();
  private final Control control;
  //Flexibility Intermediate
  private final Flexibility intermediateFlex = new Flexibility(null);
  private final JPopupMenu menu = new JPopupMenu();
  private final JMenuItem priorityItem = new JMenuItem("EditPriorities");
  private final JMenuItem flexItem = new JMenuItem("AddFlex");
  //Tabs
  String gridTabString = "Grid";
  String orderTabString = "Order";
  private JPanel nothingSelectedPanel;
  private JPanel selectedPanel;
  private JScrollPane usageViewPanel;
  private boolean offered = true, onConstrain = true, offConstrain = false;
  //JTree
  private DefaultMutableTreeNode listOfAllSelectedHolonObjects;
  private JTree stateTree;
  private DefaultTreeModel treeModel;
  Runnable update = this::update;

  /**
   * Construct the window.
   * @param parentFrame parent
   * @param control control
   */
  public FlexWindow(JFrame parentFrame, Control control) {
    this.intermediateFlex.name = "name";
    this.control = control;
    //InitWindow
    createMenuBar();
    initWindowPanel(parentFrame);
    this.addWindowListener(
        (WindowClosingListener) e -> control.OnCanvasUpdate.removeListener(update));
    updateSelectedPanel();
    control.OnCanvasUpdate.addListener(update);
  }

  /**
   * Init panel
   * @param parentFrame parent
   */
  private void initWindowPanel(JFrame parentFrame) {
    this.setBounds(0, 0, 400,
        parentFrame.getHeight() > 20 ? parentFrame.getHeight() - 20 : parentFrame.getHeight());
    this.setIconImage(Import.loadImage(ImagePreference.Logo, 30, 30));
    this.setTitle("Flexibility");
    this.setLocationRelativeTo(parentFrame);
    this.setVisible(true);
    //this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    createNothingSelectedPanel();
    createSelectedPanel();
    createUsageViewPanel();
    contentPanel.addTab(gridTabString, nothingSelectedPanel);
    contentPanel.addTab(orderTabString, usageViewPanel);
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    this.setContentPane(contentPanel);
    this.revalidate();

  }

  /**
   * Creates the menu bar of this frame.
   */
  private void createMenuBar() {
    JMenuBar menuBar = new JMenuBar();
    JMenu canvas = new JMenu("Canvas");
    menuBar.add(canvas);
    JMenuItem updateMenuItem = new JMenuItem("Update");
    updateMenuItem.addActionListener(clicked -> updateSelectedPanel());
    canvas.add(updateMenuItem);
    JMenu flex = new JMenu("Flex");
    menuBar.add(flex);
    JMenuItem addMenuItem = new JMenuItem("Add Flexibility");
    addMenuItem.addActionListener(clicked -> createAddDialog(null));
    flex.add(addMenuItem);
    JMenuItem deleteMenuItem = new JMenuItem("Delete Flexibility");
    deleteMenuItem.addActionListener(clicked -> createDeleteDialog());
    flex.add(deleteMenuItem);

    this.setJMenuBar(menuBar);
  }


  private void createUsageViewPanel() {
    JPanel panel = new JPanel(new GridBagLayout());
    usageViewPanel = new JScrollPane(panel);
    panel.setBackground(Color.white);
    FlexState[] titles = FlexState.values();

    for (int i = 0; i < 5; i++) {
      final int titleIndex = i;
      List<Flexibility> flexList = control.getModel().getAllFlexibilities();
      List<Flexibility> listOfFlexWithState = flexList.stream()
          .filter(flex -> flex.getState().equals(titles[titleIndex])).toList();
      JLabel label = new JLabel(titles[i].toString() + "[" + listOfFlexWithState.size() + "]");
      GridBagConstraints labelC = new GridBagConstraints();
      labelC.gridx = 1;
      labelC.gridy = i * 2;
      labelC.anchor = GridBagConstraints.LINE_START;
      labelC.fill = GridBagConstraints.HORIZONTAL;
      labelC.weightx = 0.5;
      labelC.weighty = 0.0;
      panel.add(label, labelC);

      JPanel listPanel = new JPanel(new GridBagLayout());
      createFlexPanel(listPanel, listOfFlexWithState);
      GridBagConstraints panelC = new GridBagConstraints();
      panelC.gridx = 0;
      panelC.gridwidth = 2;
      panelC.gridy = i * 2 + 1;
      panelC.fill = GridBagConstraints.BOTH;
      panel.add(listPanel, panelC);

      JButton expandButton = new JButton("-");
      GridBagConstraints buttonC = new GridBagConstraints();
      buttonC.gridx = 0;
      buttonC.gridy = i * 2;
      panel.add(expandButton, buttonC);
      expandButton.addActionListener(clicked -> {
        listPanel.setVisible(!listPanel.isVisible());
        expandButton.setText(listPanel.isVisible() ? "-" : "+");
      });

    }
    //Add Spacer
    JLabel spacer = new JLabel();
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 5 * 2;
    c.fill = GridBagConstraints.VERTICAL;
    c.weightx = 0.0;
    c.weighty = 1;
    panel.add(spacer, c);
  }


  private void createFlexPanel(JPanel listPanel, List<Flexibility> flexList) {
    listPanel.setBackground(Color.white);
    Insets insets = new Insets(2, 2, 2, 2);
    int panelWidth = Math.max(boxSideLength, this.getWidth() - 90);
    int maxButtonsPerLine = panelWidth / boxSideLength;

    int i = 0;
    for (Flexibility flex : flexList) {
      GridBagConstraints c = new GridBagConstraints();
      c.gridx = Math.floorMod(i, maxButtonsPerLine);
      c.weightx = 0.0;
      c.insets = insets;
      JButton labelButton = new JButton(flex.name);
      labelButton.setPreferredSize(new Dimension(boxSideLength, boxSideLength));
      labelButton.setBorder(BorderFactory.createLineBorder(Color.black));
      listPanel.add(labelButton, c);
      labelButton.addActionListener(clicked -> {
        flex.order();
        control.calculateStateForCurrentIteration();
      });
      labelButton.setToolTipText(createToolTipText(flex));
      i++;
    }
    //AddSpacer
    JLabel spacer = new JLabel();
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = maxButtonsPerLine;
    c.gridy = 0;
    c.fill = GridBagConstraints.VERTICAL;
    c.weightx = 1;
    c.weighty = 0;

    listPanel.add(spacer, c);
  }


  /**
   * Create a tooltip text for a flexibility.
   * @param actual flexibility
   * @return description
   */
  private String createToolTipText(Flexibility actual) {
    return "<html>" +
        "<b>" + actual.name + "( </b>" + actual.getElement().getName() + "<b> )</b><br>"
        + ((actual.remainingDuration() != 0) ? "<i>Remaining Duration:" + actual.remainingDuration()
        + "</i><br>" : "")
        + ((actual.remainingTimeTillActivation() != 0) ? "<i>Remaining TimeTillActivation:"
        + actual.remainingTimeTillActivation() + "</i><br>" : "")
        + "Duration: " + actual.getDuration() + "<br>"
        + "Cooldown: " + actual.getCooldown() + "<br>"
        + "Cost: " + actual.cost + "<br>"
        + "EnergyReleased: " + actual.energyReleased() + "<br>"
        + "Constrains: " + actual.constrainList.stream().map(Constrain::getName)
        .collect(Collectors.joining(",")) + "<br>"
        + "</html>";
  }


  /**
   * Updates the panel
   */
  private void update() {
    updateSelectedPanel();
    createUsageViewPanel();
    contentPanel.setComponentAt(contentPanel.indexOfTab(orderTabString), usageViewPanel);
    contentPanel.revalidate();
  }


  private void createSelectedPanel() {
    listOfAllSelectedHolonObjects = new DefaultMutableTreeNode("HolonObjects");
    treeModel = new DefaultTreeModel(listOfAllSelectedHolonObjects);
    stateTree = new JTree(treeModel);
    JPopupMenu menu = createPopUpMenu();

    stateTree.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {

          TreePath pathUnderCursor = stateTree.getPathForLocation(e.getX(), e.getY());
          Rectangle pathBounds = stateTree.getUI().getPathBounds(stateTree, pathUnderCursor);
          if (pathBounds != null && pathBounds.contains(e.getX(), e.getY())) {
            TreePath[] selectedPaths = stateTree.getSelectionPaths();
            if (selectedPaths == null) {
              stateTree.addSelectionPath(pathUnderCursor);
            } else {
              boolean isInSelectedPaths = false;
              for (TreePath path : stateTree.getSelectionPaths()) {
                if (path.equals(pathUnderCursor)) {
                  isInSelectedPaths = true;
                  break;
                }
              }
              if (!isInSelectedPaths) {
                stateTree.clearSelection();
                stateTree.addSelectionPath(pathUnderCursor);
              }
            }

            menu.show(stateTree, pathBounds.x, pathBounds.y + pathBounds.height);
          }
        }
      }
    });
    selectedPanel = new JPanel(new BorderLayout());
    selectedPanel.add(new JScrollPane(stateTree));
  }

  private JPopupMenu createPopUpMenu() {
    priorityItem.addActionListener(clicked -> getInfoFromSelection().ifPresent(eleInfo -> {
      Priority priority = (Priority) JOptionPane.showInputDialog(stateTree,
          "Select the Priority:", "Priority?", JOptionPane.INFORMATION_MESSAGE,
          new ImageIcon(new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)),
          Priority.values(), "");
      if (priority == null) {
        return;
      }
      eleInfo.ele.setPriority(priority);
      control.updateStateForCurrentIteration();
    }));
    flexItem.addActionListener(
        clicked -> getInfoFromSelection().ifPresent(eleInfo -> createAddDialog(eleInfo.ele)));
    menu.add(priorityItem);
    menu.add(flexItem);
    return menu;
  }

  private Optional<ElementInfo> getInfoFromSelection() {
    TreePath path = stateTree.getSelectionPath();
    if (path == null) {
      return Optional.empty();
    }
    Object userObject = ((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
    if (userObject instanceof ElementInfo eleInfo) {
      return Optional.of(eleInfo);
    }
    return Optional.empty();
  }


  private void createNothingSelectedPanel() {
    nothingSelectedPanel = new JPanel();
    nothingSelectedPanel.setLayout(new BoxLayout(nothingSelectedPanel, BoxLayout.PAGE_AXIS));
    JLabel nothingSelectedTextLabel = new JLabel("No HolonObject exist.");
    nothingSelectedTextLabel.setForeground(Color.gray);
    nothingSelectedTextLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    nothingSelectedPanel.add(Box.createVerticalGlue());
    nothingSelectedPanel.add(nothingSelectedTextLabel);
    nothingSelectedPanel.add(Box.createVerticalGlue());
  }

  private void updateSelectedPanel() {
    //Should not remove add so many Components

    listOfAllSelectedHolonObjects.removeAllChildren();
    //Init with HolonObjects
    control.getModel().getCanvas().getObjectsInThisLayer().forEach(aCps -> {
      DefaultMutableTreeNode newObjectChild = new DefaultMutableTreeNode(
          aCps.getName() + " ID:" + aCps.getId());
      if (aCps instanceof HolonObject hO) {
        expandTreeHolonObject(hO, newObjectChild);
      }
      if (aCps instanceof GroupNode groupnode) {
        expandTreeUpperNode(groupnode, newObjectChild);
      }
      listOfAllSelectedHolonObjects.add(newObjectChild);
    });
    treeModel.nodeStructureChanged(listOfAllSelectedHolonObjects);

    stateTree.revalidate();
    expandAll(stateTree);
    selectedPanel.revalidate();
    contentPanel.setComponentAt(contentPanel.indexOfTab(gridTabString), selectedPanel);
    contentPanel.revalidate();
    this.revalidate();
  }

  /**
   * Helper function to expand the branches of a JTree.
   * @param tree the JTree
   */
  private void expandAll(JTree tree) {
    for (int i = 0; i < tree.getRowCount(); i++) {
      tree.expandRow(i);
    }
  }


  private void expandTreeUpperNode(GroupNode groupNode, DefaultMutableTreeNode root) {
    groupNode.getHolonObjects().forEach(object -> {
      DefaultMutableTreeNode newObjectChild = new DefaultMutableTreeNode(
          object.getName() + " ID:" + object.getId());
      root.add(newObjectChild);
    });
    groupNode.getGroupNodes().forEach(childGroupNode -> {
      DefaultMutableTreeNode newObjectChild = new DefaultMutableTreeNode(
          childGroupNode.getName() + " ID:" + childGroupNode.getId());
      expandTreeUpperNode(childGroupNode, newObjectChild);
      root.add(newObjectChild);
    });
  }


  private void expandTreeHolonObject(HolonObject hObject, DefaultMutableTreeNode root) {
    hObject.elementsStream().forEach(hE -> {
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(new ElementInfo(hE));
      expandTreeFlex(hE, newChild);
      root.add(newChild);
    });
  }

  private void expandTreeFlex(HolonElement hElement, DefaultMutableTreeNode root) {
    for (Flexibility flex : hElement.flexList) {
      String flexState;
      Color color = ColorPreference.Flexibility.getStateColor(flex.getState());
      flexState = "<font bgcolor='#" + Integer.toHexString(color.getRGB()).substring(2) + "'>"
          + flex.getState().name() + "</font>";
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(
          "<html>" + flexState + " <b>" + flex.name + "</b>" + "</html>");
      root.add(newChild);
    }
  }


  private void createDeleteDialog() {
    Object[] allFlexes = control.getModel().getAllFlexibilities().toArray();
    if (allFlexes.length == 0) {
      JOptionPane.showMessageDialog(this,
          "No Flexibility exist.",
          "Warning",
          JOptionPane.WARNING_MESSAGE);
      return;
    }

    Flexibility toDeleteFlex = (Flexibility) JOptionPane.showInputDialog(this,
        "Select to Delete Flexibility:", "Flexibility?", JOptionPane.INFORMATION_MESSAGE,
        new ImageIcon(new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)), allFlexes, "");
    if (toDeleteFlex != null) {
      toDeleteFlex.getElement().flexList.remove(toDeleteFlex);
      control.calculateStateForCurrentIteration();
      updateSelectedPanel();
    }
  }


  //Add Element
  private void createAddDialog(HolonElement element) {
    if (control.getModel().getCanvas().getAllHolonObjectsRecursive().findAny().isEmpty()) {
      JOptionPane.showMessageDialog(this,
          "No HolonObject exist.",
          "Warning",
          JOptionPane.WARNING_MESSAGE);
      return;
    }
    JDialog addDialog = new JDialog();
    addDialog.setTitle("Create Flexibility");
    addDialog.setBounds(0, 0, 820, 400);
    addDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    JPanel dialogPanel = new JPanel(new BorderLayout());
    addDialog.setContentPane(dialogPanel);
    JPanel selectionPanel = new JPanel(null);
    dialogPanel.add(selectionPanel, BorderLayout.CENTER);
    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    dialogPanel.add(buttonPanel, BorderLayout.PAGE_END);

    addDialog.setModalityType(ModalityType.APPLICATION_MODAL);

    //Create HolonObject selection Box
    HolonObject[] holonObjects = control.getModel().getCanvas().getAllHolonObjectsRecursive()
        .toArray(HolonObject[]::new);

    DefaultComboBoxModel<HolonObject> comboBoxModel = new DefaultComboBoxModel<>(holonObjects);

    JComboBox<HolonObject> holonObjectSelector = new JComboBox<>(comboBoxModel);
    holonObjectSelector.setBounds(10, 30, 800, 30);

    DefaultComboBoxModel<HolonElement> comboBoxModelElements = new DefaultComboBoxModel<>(
        holonObjects[0].elementsStream().toArray(HolonElement[]::new));
    JComboBox<HolonElement> holonElementSelector = new JComboBox<>(comboBoxModelElements);
    holonElementSelector.setBounds(10, 80, 800, 30);

    holonObjectSelector.addItemListener(aListener -> {
      if (aListener.getStateChange() == ItemEvent.SELECTED) {
        DefaultComboBoxModel<HolonElement> newComboBoxModelElements = new DefaultComboBoxModel<>(
            ((HolonObject) aListener.getItem()).elementsStream().toArray(HolonElement[]::new));
        holonElementSelector.setModel(newComboBoxModelElements);
      }
    });

    if (element == null) {
      selectionPanel.add(holonObjectSelector);
      selectionPanel.add(holonElementSelector);
      JLabel selectObjectLabel = new JLabel("Select HolonObject:");
      selectObjectLabel.setBounds(10, 10, 200, 20);
      selectionPanel.add(selectObjectLabel);
      JLabel selectElementLabel = new JLabel("Select HolonElement:");
      selectElementLabel.setBounds(10, 60, 200, 20);
      selectionPanel.add(selectElementLabel);
    } else {
      JLabel selectElementLabel = new JLabel("Selected: " + element);
      selectElementLabel.setBounds(10, 60, 2000, 20);
      selectionPanel.add(selectElementLabel);
    }

    JPanel flexAttributesBorderPanel = new JPanel(null);
    flexAttributesBorderPanel.setBounds(10, 120, 800, 200);
    flexAttributesBorderPanel.setBorder(BorderFactory.createTitledBorder("Flexibility Attributes"));
    selectionPanel.add(flexAttributesBorderPanel);
    JLabel flexNameLabel = new JLabel("Name:");
    flexNameLabel.setBounds(10, 20, 50, 20);
    flexAttributesBorderPanel.add(flexNameLabel);
    JFormattedTextField nameTextField = new JFormattedTextField(intermediateFlex.name);
    nameTextField.addPropertyChangeListener(
        changed -> intermediateFlex.name = nameTextField.getText());
    nameTextField.setBounds(80, 15, 200, 30);
    flexAttributesBorderPanel.add(nameTextField);
    JLabel flexSpeedLabel = new JLabel("Speed:");
    flexSpeedLabel.setBounds(10, 55, 50, 20);
    flexAttributesBorderPanel.add(flexSpeedLabel);
    //Integer formatter
    NumberFormat format = NumberFormat.getIntegerInstance();
    format.setGroupingUsed(false);
    format.setParseIntegerOnly(true);
    NumberFormatter integerFormatter = new NumberFormatter(format);
    integerFormatter.setMinimum(0);
    integerFormatter.setCommitsOnValidEdit(true);

    JFormattedTextField speedTextField = new JFormattedTextField(integerFormatter);
    speedTextField.setValue(intermediateFlex.speed);
    speedTextField.setToolTipText("Only positive Integer.");
    speedTextField.addPropertyChangeListener(
        actionEvent -> intermediateFlex.speed = Integer.parseInt(
            speedTextField.getValue().toString()));
    speedTextField.setBounds(80, 50, 200, 30);
    flexAttributesBorderPanel.add(speedTextField);
    speedTextField.setEnabled(false);

    JLabel flexDurationLabel = new JLabel("Duration:");
    flexDurationLabel.setBounds(10, 90, 70, 20);
    flexAttributesBorderPanel.add(flexDurationLabel);

    NumberFormatter moreThenZeroIntegerFormatter = new NumberFormatter(format);
    moreThenZeroIntegerFormatter.setMinimum(1);
    moreThenZeroIntegerFormatter.setCommitsOnValidEdit(true);

    JFormattedTextField durationTextField = new JFormattedTextField(moreThenZeroIntegerFormatter);
    durationTextField.setValue(intermediateFlex.getDuration());
    durationTextField.setToolTipText("Only positive Integer bigger then 0.");
    durationTextField.addPropertyChangeListener(actionEvent -> intermediateFlex.setDuration(
        Integer.parseInt(durationTextField.getValue().toString())));
    durationTextField.setBounds(80, 85, 200, 30);
    flexAttributesBorderPanel.add(durationTextField);

    JLabel flexCostsLabel = new JLabel("Costs:");
    flexCostsLabel.setBounds(10, 125, 70, 20);
    flexAttributesBorderPanel.add(flexCostsLabel);

    //Double Format:
    NumberFormat doubleFormat = NumberFormat.getNumberInstance(Locale.US);
    doubleFormat.setMinimumFractionDigits(1);
    doubleFormat.setMaximumFractionDigits(2);
    doubleFormat.setRoundingMode(RoundingMode.HALF_UP);

    //CostFormatter:
    NumberFormatter costsFormatter = new NumberFormatter(doubleFormat);
    costsFormatter.setMinimum(0.0);

    JFormattedTextField costTextField = new JFormattedTextField(costsFormatter);
    costTextField.setValue(intermediateFlex.cost);
    costTextField.setToolTipText("Only non negative Double with DecimalSeparator Point('.').");
    costTextField.addPropertyChangeListener(
        propertyChange -> intermediateFlex.cost = Float.parseFloat(
            costTextField.getValue().toString()));
    costTextField.setBounds(80, 120, 200, 30);
    flexAttributesBorderPanel.add(costTextField);

    JLabel flexCooldownLabel = new JLabel("Cooldown:");
    flexCooldownLabel.setBounds(310, 20, 70, 20);
    flexAttributesBorderPanel.add(flexCooldownLabel);

    JFormattedTextField cooldownTextField = new JFormattedTextField(moreThenZeroIntegerFormatter);
    cooldownTextField.setValue(intermediateFlex.getCooldown());
    cooldownTextField.setToolTipText("Only positive Integer.");
    cooldownTextField.addPropertyChangeListener(actionEvent -> intermediateFlex.setCooldown(
        Integer.parseInt(cooldownTextField.getValue().toString())));
    cooldownTextField.setBounds(380, 15, 200, 30);
    flexAttributesBorderPanel.add(cooldownTextField);

    JCheckBox offeredCheckBox = new JCheckBox("Offered");
    offeredCheckBox.setSelected(this.offered);
    offeredCheckBox.setBounds(310, 55, 200, 20);
    flexAttributesBorderPanel.add(offeredCheckBox);

    JCheckBox onConstrainCheckBox = new JCheckBox("On_Constrain");
    onConstrainCheckBox.setSelected(this.onConstrain);
    onConstrainCheckBox.setBounds(310, 80, 200, 20);
    flexAttributesBorderPanel.add(onConstrainCheckBox);

    JCheckBox offConstrainCheckBox = new JCheckBox("Off_Constrain");
    offConstrainCheckBox.setSelected(this.offConstrain);
    offConstrainCheckBox.setBounds(310, 105, 200, 20);
    flexAttributesBorderPanel.add(offConstrainCheckBox);

    //Both cant be true....
    onConstrainCheckBox.addActionListener(clicked -> {
      if (onConstrainCheckBox.isSelected()) {
        offConstrainCheckBox.setSelected(false);
      }
    });
    offConstrainCheckBox.addActionListener(clicked -> {
      if (offConstrainCheckBox.isSelected()) {
        onConstrainCheckBox.setSelected(false);
      }
    });

    JButton createFlexButton = new JButton("Create");
    createFlexButton.addActionListener(clicked -> {
      HolonElement ele;
      if (element == null) {
        ele = (HolonElement) holonElementSelector.getSelectedItem();
      } else {
        ele = element;
      }
      Flexibility toCreateFlex = new Flexibility(ele);
      toCreateFlex.name = intermediateFlex.name;
      toCreateFlex.speed = intermediateFlex.speed;
      toCreateFlex.setDuration(intermediateFlex.getDuration());
      toCreateFlex.cost = intermediateFlex.cost;
      toCreateFlex.setCooldown(intermediateFlex.getCooldown());
      toCreateFlex.offered = offeredCheckBox.isSelected();
      if (onConstrainCheckBox.isSelected()) {
        toCreateFlex.constrainList.add(Constrain.createOnConstrain());
      }
      if (offConstrainCheckBox.isSelected()) {
        toCreateFlex.constrainList.add(Constrain.createOffConstrain());
      }

      assert ele != null;
      ele.flexList.add(toCreateFlex);
      //save checkboxes
      this.offered = offeredCheckBox.isSelected();
      this.onConstrain = onConstrainCheckBox.isSelected();
      this.offConstrain = offConstrainCheckBox.isSelected();

      control.updateStateForCurrentIteration();
      addDialog.dispose();
    });
    buttonPanel.add(createFlexButton);
    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(clicked -> addDialog.dispose());
    buttonPanel.add(cancelButton);

    //last
    addDialog.setLocationRelativeTo(this);
    addDialog.setVisible(true);
  }

  /**
   * Wrapper class for a Element
   */
  static class ElementInfo {
    HolonElement ele;
    public ElementInfo(HolonElement ele) {
      this.ele = ele;
    }
    @Override
    public String toString() {
      return ele.getName() + " Priority:" + ele.getPriority();
    }
  }


}
