package holeg.ui.view;

import holeg.model.Model;
import holeg.ui.controller.Control;
import holeg.ui.view.main.Gui;
import java.awt.EventQueue;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * The main entry of this program.
 */
public class Main {
  private static final LogManager logManager = LogManager.getLogManager();
  private static final Logger log = Logger.getLogger(Main.class.getName());

  static {
    //Initialize logging
    try {
      logManager.readConfiguration(new FileInputStream("./config/log.properties"));
    } catch (IOException exception) {
      log.log(Level.SEVERE, "Error in loading configuration", exception);
    }
  }

  /**
   * Main method of this program.
   * No arguments are evaluated.
   * Sets the look and feel, creates the gui and start the EventQueue.
   * @param args arguments
   */
  public static void main(String[] args) {
    setLookAndFeel();
    setLocale();
    EventQueue.invokeLater(() -> {
      Model model = new Model();
      Control control = new Control(model);
      Gui view = new Gui(control);
      control.loadCategory();
      view.setVisible(true);
    });
  }

  /**
   * Sets the locale.
   */
  private static void setLocale() {
    Locale.setDefault(Locale.US);
  }

  /**
   * This method loads the System LookAndFeel. Except for Linux OS.
   */
  private static void setLookAndFeel() {
    try {
      if (!System.getProperty("os.name").startsWith("Linux")) {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      }
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | UnsupportedLookAndFeelException ignored) {
    }
  }

}
