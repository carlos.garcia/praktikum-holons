package holeg.ui.view.canvas;

import holeg.model.GroupNode;
import holeg.preferences.ImagePreference;
import holeg.ui.controller.Control;
import holeg.ui.model.GuiSettings;
import holeg.ui.view.image.Import;
import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

public class CanvasCollectionPanel extends JTabbedPane {

  private final Control control;
  private final Map<Component, Canvas> componentCanvasMap = new HashMap<>();
  private final Map<GroupNode, Component> groupNodeComponentMap = new HashMap<>();
  private Canvas main;
  private Canvas actual;

  public CanvasCollectionPanel(Control control) {
    this.control = control;
    control.OnShowGroupNode.addListener(this::showGroupNode);
    control.OnRemoveGroupNode.addListener(this::removeGroupNode);
    control.OnModelChanged.addListener(this::reset);
    createMainCanvas();
    this.addChangeListener(this::tabChangeEvent);
  }

  private void reset() {
    removeAll();
    componentCanvasMap.clear();
    groupNodeComponentMap.clear();
    createMainCanvas();
  }


  public void resetCanvasSizes() {
    Dimension dimension = new Dimension(GuiSettings.canvasSize.getX(),
        GuiSettings.canvasSize.getY());
    for (Map.Entry<Component, Canvas> entry : componentCanvasMap.entrySet()) {
      entry.getValue().setPreferredSize(dimension);
      entry.getKey().revalidate();
    }
  }

  private void tabChangeEvent(ChangeEvent changeEvent) {
    Optional.ofNullable(componentCanvasMap.get(this.getSelectedComponent()))
        .ifPresentOrElse(canvas -> {
          actual = canvas;
        }, () -> actual = main);
    control.clearSelection();
  }

  private void createMainCanvas() {
    GroupNode mainGroupNode = control.getModel().getCanvas();
    Canvas canvas = new Canvas(control, mainGroupNode);
    actual = main = canvas;
    final JScrollPane scrollPane = new JScrollPane(canvas);
    groupNodeComponentMap.put(mainGroupNode, scrollPane);
    componentCanvasMap.put(scrollPane, canvas);
    this.addTab("Main", scrollPane);
  }

  private void showGroupNode(GroupNode groupNode) {
    Optional.ofNullable(groupNodeComponentMap.get(groupNode))
        .ifPresentOrElse(this::setSelectedComponent,
            () -> createNewCanvas(groupNode));
  }

  private void createNewCanvas(GroupNode groupNode) {
    Canvas canvas = new Canvas(control, groupNode);
    final JScrollPane scrollPane = new JScrollPane(canvas);
    groupNodeComponentMap.put(groupNode, scrollPane);
    componentCanvasMap.put(scrollPane, canvas);
    addTab(groupNode.getName(), scrollPane);
    setSelectedComponent(scrollPane);
    setTabComponentAt(this.indexOfComponent(scrollPane), new GroupNodeHeader(groupNode));
  }

  private void removeGroupNode(GroupNode groupNode) {
    Optional.ofNullable(groupNodeComponentMap.get(groupNode))
        .ifPresent(scrollPane -> {
          remove(scrollPane);
          groupNodeComponentMap.remove(groupNode);
        });
  }

  public Canvas getActualCanvas() {
    return actual;
  }

  private class GroupNodeHeader extends JPanel {

    public GroupNodeHeader(GroupNode groupNode) {
      JLabel label = new JLabel(groupNode.getName());
      add(label);
      label.setBorder(null);
      label.setPreferredSize(new Dimension(label.getPreferredSize().width, 8));
      this.setBorder(null);
      this.setOpaque(false);
      JButton button = new JButton(
          new ImageIcon(Import.loadImage(ImagePreference.Button.GroupNode.Close, 8, 8)));
      button.setPreferredSize(new Dimension(8, 8));
      button.setBorder(null);
      add(button);
      button.addActionListener(clicked -> removeGroupNode(groupNode));
    }
  }
}
