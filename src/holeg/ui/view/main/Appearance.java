package holeg.ui.view.main;

import holeg.preferences.PreferenceKeys;
import java.util.prefs.Preferences;

/**
 * Class for storing the appearance flags chosen by the user.
 */
public class Appearance {
  private static final Preferences prefs = Preferences.userNodeForPackage(Appearance.class);
  public static boolean supplyBarVisible = true;
  public static boolean edgeCapacityVisible = true;
  public static boolean canvasObjectEnergyVisible = true;

  public static void loadPrefs() {
    supplyBarVisible = prefs.getBoolean(PreferenceKeys.Appearance.SupplyBarVisible, true);
    edgeCapacityVisible = prefs.getBoolean(PreferenceKeys.Appearance.EdgeCapacityVisible, true);
    canvasObjectEnergyVisible = prefs.getBoolean(PreferenceKeys.Appearance.EdgeCapacityVisible,
        true);
  }

  public static void savePrefs() {
    prefs.putBoolean(PreferenceKeys.Appearance.SupplyBarVisible, supplyBarVisible);
    prefs.putBoolean(PreferenceKeys.Appearance.EdgeCapacityVisible, edgeCapacityVisible);
    prefs.putBoolean(PreferenceKeys.Appearance.CanvasObjectEnergyVisible,
        canvasObjectEnergyVisible);
  }
}
