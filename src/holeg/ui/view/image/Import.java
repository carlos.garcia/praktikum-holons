package holeg.ui.view.image;

import holeg.preferences.ImagePreference;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 * Centralized resource loading methods for improved
 * performance and easier troubleshooting.
 */
public class Import {

  /*
   * Save nothing: Reload every image whenever requested. Save external: Only save
   * images that were found as external file. Save raw: Save all non-scaled
   * images. Save everything: self-explanatory.
   */
  private static final HashMap<String, Image> imgStorage;

  static {
    imgStorage = new HashMap<>();
  }

  private Import() {
  }

  /**
   * The rawest function replacing the old method without any default parameters. Currently not used
   * in HOLEG at all(aside form calls from the convenience methods).
   *
   * @param url   Path to the image to be loaded.
   * @param w     Width the loaded image should be scaled to.
   * @param h     Height the loaded image should be scaled to.
   * @param hints Hints for the scaling algorithm to be used. Same as the parameter in the
   *              getScaledInstance function of Image.
   * @return A loaded and scaled image from the requested path.
   */
  static Image loadImage(String url, int w, int h, int hints) {
    String key = url + "?" + w + "?" + h + "?" + hints;
    if (imgStorage.containsKey(key)) {
      return (imgStorage.get(key));
    } else {
      Image img = loadImage(url).getScaledInstance(w, h, hints);
      imgStorage.put(key, img);
      return img;
    }
  }

  /**
   * Loads an image from the given path and scales it using the smooth algorithm.
   *
   * @param url Path to the image to be loaded.
   * @param w   Width the loaded image should be scaled to.
   * @param h   Height the loaded image should be scaled to.
   * @return A loaded and (smoothly) scaled image from the requested path.
   */
  public static Image loadImage(String url, int w, int h) {
    return loadImage(url, w, h, Image.SCALE_SMOOTH);
  }

  /**
   * Loads an image from the given path and scales it using the smooth algorithm.
   *
   * @param url Path to the image to be loaded.
   * @return An image loaded from the requested path.
   */
  public static Image loadImage(String url) {
    if (imgStorage.containsKey(url)) {
      return imgStorage.get(url);
    } else {
      Image img;
      try {
        img = ImageIO.read(loadStream(url));
      } catch (IOException e) {
        return new BufferedImage(0, 0,
            BufferedImage.TYPE_INT_RGB);
      }
      imgStorage.put(url, img);
      return img;
    }
  }

  /**
   * Loads any resource with a given path, regardless of whether it is inside the jar or an external
   * resource. Every loadImage() function uses this as a basis.
   *
   * @param url The path (and file name) of the requested resource.
   * @return An InputStream from the requested resource.
   * @throws FileNotFoundException if file cannot be found
   */
  public static InputStream loadStream(String url) throws FileNotFoundException {
    InputStream o = InputStream.class.getResourceAsStream(url);
    if (o != null) {
      return o;
    } else {
      boolean rootSymbol = switch (url.charAt(
          0)) { // So we can make sure to construct res/path correctly.
        case '/', '\\' -> true;
        default -> false; // Whether url starts with a / or \
      };
      File f = new File(url);
      if (!f.exists()) {
        url = "res" + (rootSymbol ? "" : "/") + url;
      }
      f = new File(url);// Possible bug with duplicate names.
      if (!f.exists()) {
        url = "res" + ImagePreference.Canvas.ImageNotFound;
      }
      return new FileInputStream(url);

    }
  }
}