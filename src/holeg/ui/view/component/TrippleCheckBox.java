package holeg.ui.view.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.UIManager;

/**
 * A checkbox with tree states. Specially for the indication that none, all or partial amount of
 * things are selected.
 */
public class TrippleCheckBox extends JCheckBox implements Icon, ActionListener {

  final static Icon icon = UIManager.getIcon("CheckBox.icon");
  private static final Color borderColor = new Color(81, 81, 81);
  private static final Color enabledBackgroundColor = new Color(51, 51, 51);
  private static final Color disabledBackgroundColor = new Color(122, 138, 153);
  State state;

  public TrippleCheckBox() {
    this("", State.unselected);
  }


  public TrippleCheckBox(String text) {
    this(text, State.unselected);
  }

  public TrippleCheckBox(String text, State selection) {
    super(text, selection == State.selected);
    setSelectionState(selection);
    addActionListener(this);
    setIcon(this);
  }

  @Override
  public boolean isSelected() {
    return (getSelectionState() == State.selected);
  }

  public State getSelectionState() {
    return state;
  }

  public void setSelectionState(State selection) {
    state = selection;
    setSelected(state == State.selected);
    repaint();
  }

  @Override
  public void paintIcon(Component c, Graphics g, int x, int y) {
    icon.paintIcon(c, g, x, y);
    if (getSelectionState() != State.mid_state_selection) {
      return;
    }

    int w = getIconWidth();
    int h = getIconHeight();
    g.setColor(Color.white);
    g.fillRect(x + 1, y + 1, w - 2, h - 2);
    g.setColor(c.isEnabled() ? enabledBackgroundColor : disabledBackgroundColor);
    g.fillRect(x + 4, y + 4, w - 8, h - 8);

    if (!c.isEnabled()) {
      return;
    }
    g.setColor(borderColor);
    g.drawRect(x + 4, y + 4, w - 9, h - 9);
  }

  @Override
  public int getIconWidth() {
    return icon.getIconWidth();
  }

  @Override
  public int getIconHeight() {
    return icon.getIconHeight();
  }

  public void actionPerformed(ActionEvent e) {
    switch (getSelectionState()) {
      case unselected:
        setSelectionState(State.selected);
        break;
      case mid_state_selection:
        setSelectionState(State.unselected);
        break;
      case selected:
        setSelectionState(State.unselected);
        break;
    }
  }

  public enum State {
    unselected, mid_state_selection, selected
  }
}