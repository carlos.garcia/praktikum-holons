package holeg.ui.view.component;

import holeg.preferences.ImagePreference;
import holeg.ui.view.image.Import;
import java.awt.BorderLayout;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.text.DefaultCaret;

/**
 * Little new swing object to print data to a console.
 */
public class Console extends JPanel {

  private final JTextArea textArea = new JTextArea();

  public Console() {
    super();
    this.setLayout(new BorderLayout());
    textArea.setEditable(false);
    DefaultCaret caret = (DefaultCaret) textArea.getCaret();
    caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    JScrollPane scrollPane = new JScrollPane(textArea);
    this.add(scrollPane, BorderLayout.CENTER);
    JToolBar toolBar = new JToolBar();
    toolBar.setFloatable(false);
    JButton clearButton = new JButton("",
        new ImageIcon(Import.loadImage(ImagePreference.Button.Console.Clear, 24, 24)));
    clearButton.setToolTipText("Clear Console");
    clearButton.addActionListener(actionEvent -> clear());
    toolBar.add(clearButton);
    toolBar.add(Box.createHorizontalGlue());
    JButton topButton = new JButton("",
        new ImageIcon(Import.loadImage(ImagePreference.Button.Console.Top, 24, 24)));
    topButton.setToolTipText("Scroll to top");
    topButton.addActionListener(actionEvent -> scrollToTop());
    toolBar.add(topButton);
    JButton botButton = new JButton("",
        new ImageIcon(Import.loadImage(ImagePreference.Button.Console.Bottom, 24, 24)));
    botButton.setToolTipText("Scroll to bottom");
    botButton.addActionListener(actionEvent -> scrollToBottom());
    toolBar.add(botButton);
    scrollPane.setColumnHeaderView(toolBar);
  }

  private void scrollToTop() {
    textArea.setCaretPosition(0);
  }

  private void scrollToBottom() {
    textArea.setCaretPosition(textArea.getDocument().getLength());
  }


  public void clear() {
    textArea.setText("");
  }

  public void print(String message) {
    textArea.append(message);

  }

  public void println(String message) {
    textArea.append(message + "\n");
  }
}
