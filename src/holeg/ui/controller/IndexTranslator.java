package holeg.ui.controller;

import holeg.interfaces.LocalMode;


public class IndexTranslator {

  /**
   * Determines the index of the internal value array of an element that should be used, since
   * elements only save 100 values, but iterations and local period can be anything between 1 and
   * 100000.
   *
   * @param e        the element for which the calculation should be made.
   * @param timeStep the iteration for which the calculation should be made.
   * @return effective Index
   */
  public static int getEffectiveIndex(LocalMode e, int timeStep) {
    int period = e.getPeriod().getInterval();
    return timeStep % period * 100 / period;
  }
}
