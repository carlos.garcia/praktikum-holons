package holeg.ui.controller;

import holeg.model.AbstractCanvasObject;
import holeg.model.Edge;
import holeg.model.Edge.EdgeState;
import holeg.model.Holon;
import holeg.model.HolonObject;
import holeg.model.HolonSwitch;
import holeg.model.HolonSwitch.SwitchState;
import holeg.model.Model;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SimulationManager {

  private static final Logger log = Logger.getLogger(SimulationManager.class.getName());
  private final Control control;

  public SimulationManager(Control control) {
    log.fine("Construct SimulationManager");
    this.control = control;
  }

  public void calculateStateForIteration(int timeStep) {
    Model model = control.getModel();
    model.getCanvas().getAllSwitchObjectsRecursive().forEach(sw -> sw.calculateState(timeStep));
    List<HolonObject> holonObjectList = model.getCanvas().getAllHolonObjectsRecursive()
        .collect(Collectors.toList());
    List<Edge> edgeList = new ArrayList<>(model.getEdgesOnCanvas());
    Set<Holon> holons = getHolons(holonObjectList, edgeList, timeStep);
    holons.forEach(Holon::calculate);
    model.holons = holons;
  }


  private Set<Holon> getHolons(List<HolonObject> holonObjectList, List<Edge> edgeList,
      int timeStep) {
    Set<Holon> holons = new HashSet<>();
    for (Holon holon : calculateHolonStructure(holonObjectList, edgeList)) {
      final float energyOnCables = holon.holonObjects.stream().map(hO -> {
            hO.calculateEnergy(timeStep);
            return hO.getActualEnergy();
          })
          .filter(energy -> energy > 0.0f).reduce(0.0f, (Float::sum));
      // find the edge with the energy supplied from his two connected objects are
      // the biggest, from all cables that the network give more energy than the
      // edge capacity.
      Optional<Edge> edge = holon.edges.stream()
          .filter(e -> energyOnCables > e.maxCapacity && e.mode == Edge.EdgeMode.Normal)
          .max((lhs, rhs) -> Float.compare(lhs.getEnergyFromConneted(),
              rhs.getEnergyFromConneted()));
      edge.ifPresentOrElse(e -> {
        //Burn Cable
        e.setState(EdgeState.Burned);
        e.setActualFlow(0.0f);
        //Split Holon
        holons.addAll(
            getHolons(new ArrayList<>(holon.holonObjects), new ArrayList<>(holon.edges), timeStep));
      }, () -> {
        holon.edges.forEach(e -> e.setActualFlow(energyOnCables));
        holons.add(holon);
      });
    }
    return holons;
  }

  Set<Holon> calculateHolonStructure(List<HolonObject> holonObjectList, List<Edge> edgeList) {
    Set<Holon> holons = new HashSet<>();
    while (!holonObjectList.isEmpty()) {
      // lookAt the first holonObject and find his neighbors
      HolonObject lookAtObject = holonObjectList.get(0);
      // delete out of list
      holonObjectList.remove(0);
      // create a new Network
      Holon actualNetwork = new Holon(lookAtObject);
      // create List of neighbors
      LinkedList<AbstractCanvasObject> neighbors = new LinkedList<>();
      populateListOfNeighbors(edgeList, lookAtObject, actualNetwork, neighbors);
      while (!neighbors.isEmpty()) {
        AbstractCanvasObject lookAtNeighbor = neighbors.getFirst();
        if (lookAtNeighbor instanceof HolonObject) {
          holonObjectList.remove(lookAtNeighbor);
        }
        actualNetwork.add(lookAtNeighbor);
        // When HolonSwitch Check if closed
        if (!(lookAtNeighbor instanceof HolonSwitch sw) || sw.getState() == SwitchState.Closed) {
          populateListOfNeighbors(edgeList, lookAtNeighbor, actualNetwork, neighbors);
        }
        neighbors.removeFirst();
      }
      holons.add(actualNetwork);
    }
    for (Edge e : edgeList) {
      e.setActualFlow(0.0f);
    }
    return holons;
  }

  /**
   * Adds  neighbors.
   */
  void populateListOfNeighbors(List<Edge> edgeList, AbstractCanvasObject lookAtObject,
      Holon actualNetwork, LinkedList<AbstractCanvasObject> neighbors) {
    ListIterator<Edge> iter = edgeList.listIterator();
    while (iter.hasNext()) {
      Edge lookAtEdge = iter.next();
      if (lookAtEdge.getState() == EdgeState.Working && lookAtEdge.isConnectedTo(lookAtObject)) {
        iter.remove();
        actualNetwork.edges.add(lookAtEdge);

        // Add neighbor
        AbstractCanvasObject edgeNeighbor;
        if (lookAtEdge.getA().equals(lookAtObject)) {
          edgeNeighbor = lookAtEdge.getB();
        } else {
          edgeNeighbor = lookAtEdge.getA();
        }
        if (!neighbors.contains(edgeNeighbor)) {
          neighbors.add(edgeNeighbor);
        }
      }
    }
  }

}
