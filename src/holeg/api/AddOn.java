package holeg.api;

import holeg.ui.controller.Control;
import javax.swing.JPanel;

/**
 * Interface for a Standart Addon.
 *
 * @author Tom Troppmann
 */
public interface AddOn {

  public JPanel getPanel();

  public void setController(Control control);
}
