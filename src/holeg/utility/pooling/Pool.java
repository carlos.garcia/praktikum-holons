package holeg.utility.pooling;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * An abstract implementation of simple pooling. To reduce constructing time and garbage collection
 * when many objects are dynamically needed. The Pool keeps track of the objects that are borrowed
 * via the {@link #get()} Method.
 *
 * @param <T> Type of the pooled objects.
 */
public abstract class Pool<T> {

  int borrowedCount = 0;
  private int poolCapacity = 1000;
  private final ArrayList<T> poolList = new ArrayList<>(poolCapacity);
  private final ArrayList<T> borrowedList = new ArrayList<>(poolCapacity);

  /**
   * Constructs the Pool and populate it.
   */
  public Pool() {
    populatePool(poolCapacity);
  }

  /**
   * This method is called to create an object instance for the pool. This method is abstract to
   * handle all type of parametrized constructors of the pooled object type {@link T}.
   *
   * @return a new created instance of an object for pooling
   */
  public abstract T create();

  /**
   * Returns an object from the pool.
   *
   * @return the pooled object
   */
  public T get() {
    checkCapacity(borrowedCount);
    T poolObject = poolList.get(borrowedCount);
    borrowedCount++;
    borrowedList.add(poolObject);
    return poolObject;
  }

  /**
   * Resets the pool to start returning pooled objects from the beginning.
   * <br>
   * Attention:
   * <br>
   * All references to previous borrowed objects are still valid, but the {@link #get()} method will
   * start giving objects from the start of the pool. Be sure to don't use old references after the
   * call of this method.
   */
  public void clear() {
    borrowedList.clear();
    borrowedCount = 0;
  }

  /**
   * Returns the amount of borrowed objects.
   *
   * @return the amount of borrowed objects
   */
  public int getBorrowedCount() {
    return borrowedCount;
  }

  /**
   * Returns a stream of all objects that are currently borrowed.
   *
   * @return stream of borrowed objects
   */
  public Stream<T> getBorrowedStream() {
    return borrowedList.stream();
  }

  /**
   * Creates an amount of objects to the pool. The {@link #create()} method is called to instantiate
   * the objects.
   *
   * @param amount amount of new created objects
   */
  private void populatePool(int amount) {
    for (int i = 0; i < amount; i++) {
      poolList.add(create());
    }
  }

  /**
   * This method is called to check if the current capacity can hold the requested capacity. If the
   * current capacity cannot hold the requested capacity the actual capacity is increased to two
   * times the requested capacity.
   *
   * @param requestedCapacity the requested amount of pooled objects
   */
  private void checkCapacity(int requestedCapacity) {
    if (poolCapacity <= requestedCapacity) {
      int newSize = 2 * requestedCapacity;
      populatePool(newSize - poolCapacity);
      poolCapacity = 2 * requestedCapacity;
    }
  }

}
