package holeg.utility.events;

import java.util.HashSet;
import java.util.Set;

/**
 * This class represents an event that classes can assign functions to. When the event should occur it can be broadcast to all listener.
 */
public class Event {

  /**
   * Holds all lambdas that will trigger when the event is broadcast.
   */
  private final Set<Runnable> listeners = new HashSet<Runnable>();

  /**
   * Adds an event listener. Normally a lambda function.
   * @param listener the action that is triggered
   */
  public void addListener(Runnable listener) {
    listeners.add(listener);
  }

  /**
   * Broadcast the event.
   */
  public void broadcast() {
    listeners.forEach(Runnable::run);
  }
  /**
   * Removes an event listener.
   * @param listener the action that is triggered
   */
  public void removeListener(Runnable listener) {
    listeners.remove(listener);
  }
}
