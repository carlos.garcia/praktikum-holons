package holeg.utility.events;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * An Action is a parametrized event. Classes can assign functions to this action. When the event should occur it can be broadcast to all listener.
 * @param <T>
 */
public class Action<T> {
  /**
   * Holds all lambdas that will trigger when the event is broadcast.
   */
  private final Set<Consumer<T>> listeners = new HashSet<Consumer<T>>();
  /**
   * Adds an event listener. Normally a lambda function.
   * @param listener the action that is triggered
   */
  public void addListener(Consumer<T> listener) {
    listeners.add(listener);
  }
  /**
   * Broadcast the action.
   */
  public void removeListener(Consumer<T> listener) {
    listeners.remove(listener);
  }
  /**
   * Removes an event listener.
   * @param listener the action that is triggered
   */
  public void broadcast(T listener) {
    listeners.forEach(x -> x.accept(listener));
  }
}
