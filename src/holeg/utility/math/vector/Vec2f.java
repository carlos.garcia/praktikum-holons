package holeg.utility.math.vector;

import holeg.utility.math.Maths;

/**
 * A class representing a 2 dimensional vector with float entries.
 */
public class Vec2f {

  /**
   * The first entry.
   */
  private float x;
  /**
   * The second entry.
   */
  private float y;

  /**
   * Constructs a vector and initialized it with zeros.
   */
  public Vec2f() {
    x = y = 0;
  }

  /**
   * Constructs a vector with x and y.
   *
   * @param x the first entry
   * @param y the second entry
   */
  public Vec2f(float x, float y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Creates a copy of another vector.
   *
   * @param other the other to copy the entries from
   */
  public Vec2f(Vec2f other) {
    x = other.x;
    y = other.y;
  }

  /**
   * Creates a copy of another integer vector but the entries stores as float.
   *
   * @param other the other integer vector
   */
  public Vec2f(Vec2i other) {
    x = other.getX();
    y = other.getY();
  }

  /**
   * Creates the dot product of two vectors.
   *
   * @param v1 the first vector
   * @param v2 the second vector
   * @return the dot product
   */
  public static float dot(Vec2f v1, Vec2f v2) {
    return v1.dot(v2);
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  /**
   * Sets the first and second entry of the vector.
   *
   * @param x the first entry
   * @param y the second entry
   */
  public void set(float x, float y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Creates the dot product with another vector.
   *
   * @param other the other vector
   * @return the dot product
   */
  public float dot(Vec2f other) {
    float result = 0.0f;
    result = x * other.x + y * other.y;
    return result;
  }

  /**
   * Returns the euclidean distance to zero.
   *
   * @return distance to zero
   */
  public float getLength() {
    return (float) Math.sqrt(x * x + y * y);
  }

  /**
   * Return the euclidean distance to a other vector.
   *
   * @param other the other vector.
   * @return distance to the vector.
   */
  public float getDistance(Vec2f other) {
    return (float) Math.sqrt(getDistanceSqr(other));
  }

  /**
   * Return the squared distance to a other Position. Faster than {@link #getDistance(Vec2f)}
   * because no {@link Math#sqrt(double)} needed.
   *
   * @param other the other Position.
   * @return squared distance to the Position
   */
  public float getDistanceSqr(Vec2f other) {
    float xDistance = other.x - x;
    float yDistance = other.x - y;
    return xDistance * xDistance + yDistance * yDistance;
  }

  /**
   * Returns a new vector that is the sum of this vector and another Vector.
   *
   * @param other the other vector
   * @return the sum
   */
  public Vec2f add(Vec2f other) {
    Vec2f result = new Vec2f();
    result.x += other.x;
    result.y += other.y;
    return result;
  }

  /**
   * Add the entries of another Vector to this vector.
   *
   * @param other the other vector
   */
  public void addAssign(Vec2f other) {
    x += other.x;
    y += other.y;
  }

  /**
   * Returns a new vector that is the subtraction of this vector and another Vector.
   *
   * @param other the other vector
   * @return the subtraction
   */
  public Vec2f subtract(Vec2f other) {
    Vec2f result = new Vec2f();
    result.x -= other.x;
    result.y -= other.y;
    return result;
  }

  /**
   * Subtract the entries of another Vector to this vector.
   *
   * @param other the other vector
   */
  public void subtractAssign(Vec2f other) {
    x -= other.x;
    y -= other.y;
  }

  /**
   * Returns a new vector which entries are multiplied times a scaled factor.
   * @param scaleFactor the scale factor
   * @return the multiplication from this vector with a scale
   */
  public Vec2f multiply(float scaleFactor) {
    Vec2f result = new Vec2f();
    x *= scaleFactor;
    y *= scaleFactor;
    return result;
  }

  /**
   * Multiply this vector entries time a scale factor.
   * @param scaleFactor the scale factor
   */
  public void multiplyAssign(float scaleFactor) {
    x *= scaleFactor;
    y *= scaleFactor;
  }

  /** Returns a new vector which entries are divided with a divide factor.
   * @param divideFactor the divide factor
   * @return the division from this vector and a divide factor
   */
  public Vec2f divide(float divideFactor) {
    return this.multiply(1.0f / divideFactor);
  }

  /**
   * Divide this vector entries with a divide factor.
   * @param divideFactor the divide factor
   */
  public void divideAssign(float divideFactor) {
    multiplyAssign(1.0f / divideFactor);
  }

  /**
   * Normalizes this vector by dividing with the length of the vector.
   */
  public void normalize() {
    float len = getLength();
    if (len != 0.0f) {
      x /= len;
      y /= len;
    } else {
      this.set(0, 0);
    }
  }

  /**
   * Clamp the X Value two a upper or lower bound
   *
   * @param min lower bound
   * @param max upper bound
   */
  public void clampX(float min, float max) {
    x = Maths.clamp(x, min, max);
  }

  /**
   * Clamp the Y Value two a upper or lower bound
   *
   * @param min lower bound
   * @param max upper bound
   */
  public void clampY(float min, float max) {
    y = Maths.clamp(y, min, max);
  }

  @Override
  public String toString() {
    return "Vec2f(" + x + "," + y + ")";
  }
}
