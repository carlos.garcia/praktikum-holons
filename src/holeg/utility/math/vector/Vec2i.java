package holeg.utility.math.vector;

import holeg.utility.math.Maths;
import java.awt.Point;

/**
 * A class representing a 2 dimensional vector with integer entries.
 */
public class Vec2i {

  /**
   * The first entry.
   */
  private int x;
  /**
   * The second entry.
   */
  private int y;

  /**
   * Constructs a vector and initialized it with zeros.
   */
  public Vec2i() {
    x = y = 0;
  }

  /**
   * Constructs a vector with x and y.
   *
   * @param x the first entry
   * @param y the second entry
   */
  public Vec2i(int x, int y) {
    this.x = x;
    this.y = y;
  }
  /**
   * Creates a copy of another vector.
   *
   * @param other the other to copy the entries from
   */
  public Vec2i(Vec2i other) {
    x = other.x;
    y = other.y;
  }
  /**
   * Creates a vector from a point.
   *
   * @param point the point
   */
  public Vec2i(Point point) {
    x = point.x;
    y = point.y;
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  /**
   * Set the entries of the vector
   * @param x first entry
   * @param y second entry
   */
  public void set(int x, int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Copies the entries from another vector
   * @param other another Vector
   */
  public void set(Vec2i other) {
    x = other.x;
    y = other.y;
  }

  /**
   * Creates the dot product with another vector.
   *
   * @param other the other vector
   * @return the dot product
   */
  public float dot(Vec2i other) {
    return (float) (x * other.getX() + y * other.getY());
  }

  /**
   * Returns the euclidean distance to zero.
   *
   * @return distance to zero
   */
  public float getLength() {
    return (float) Math.sqrt(x * x + y * y);
  }

  /**
   * Return the Distance to a other vector.
   *
   * @param other the other vector.
   * @return distance to the vector.
   */
  public float getDistance(Vec2i other) {
    return (float) Math.sqrt(getSquaredDistance(other));
  }

  /**
   * Return the squared distance to a other Position. Faster than {@link #getDistance(Vec2i)}
   * because no {@link Math#sqrt(double)} needed.
   *
   * @param other the other Position.
   * @return squared distance to the Position
   */
  public float getSquaredDistance(Vec2i other) {
    int xDistance = other.getX() - x;
    int yDistance = other.getY() - y;
    return xDistance * xDistance + yDistance * yDistance;
  }
  /**
   * Returns a new vector that is the sum of this vector and another Vector.
   *
   * @param other the other vector
   * @return the sum
   */
  public Vec2i add(Vec2i other) {
    Vec2i result = new Vec2i();
    result.setX(x + other.getX());
    result.setY(y + other.getY());
    return result;
  }
  /**
   * Add the entries of another Vector to this vector.
   *
   * @param other the other vector
   */
  public void addAssign(Vec2i other) {
    x += other.getX();
    y += other.getY();
  }
  /**
   * Returns a new vector that is the subtraction of this vector and another Vector.
   *
   * @param other the other vector
   * @return the subtraction
   */
  public Vec2i subtract(Vec2i other) {
    Vec2i result = new Vec2i();
    result.setX(x - other.getX());
    result.setY(y - other.getY());
    return result;
  }
  /**
   * Subtract the entries of another Vector to this vector.
   *
   * @param other the other vector
   */
  public void subtractAssign(Vec2i other) {
    x -= other.getX();
    y -= other.getY();
  }
  /**
   * Returns a new vector which entries are multiplied times a scaled factor.
   * @param scaleFactor the scale factor
   * @return the multiplication from this vector with a scale
   */
  public Vec2i multiply(float scaleFactor) {
    Vec2i result = new Vec2i();
    result.setX((int) (x * scaleFactor));
    result.setY((int) (y * scaleFactor));
    return result;
  }
  /**
   * Multiply this vector entries time a scale factor.
   * @param scaleFactor the scale factor
   */
  public void multiplyAssign(float scaleFactor) {
    x *= scaleFactor;
    y *= scaleFactor;
  }
  /** Returns a new vector which entries are divided with a divide factor.
   * @param divideFactor the divide factor
   * @return the division from this vector and a divide factor
   */
  public Vec2i divide(float divideFactor) {
    return this.multiply(1.0f / divideFactor);
  }
  /**
   * Divide this vector entries with a divide factor.
   * @param divideFactor the divide factor
   */
  public void divideAssign(float divideFactor) {
    multiplyAssign(1.0f / divideFactor);
  }

  /**
   * Clamp the X Value two a upper or lower bound
   *
   * @param min lower bound
   * @param max upper bound
   */
  public void clampX(int min, int max) {
    x = Maths.clamp(x, min, max);
  }

  /**
   * Clamp the Y Value two a upper or lower bound
   *
   * @param min lower bound
   * @param max upper bound
   */
  public void clampY(int min, int max) {
    y = Maths.clamp(y, min, max);
  }

  @Override
  public String toString() {
    return "Vec2i(" + x + "," + y + ")";
  }

}
