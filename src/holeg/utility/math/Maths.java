package holeg.utility.math;

/**
 * A helper class for common math operations.
 * It's an extension for {@link java.lang.Math}.
 */
public class Maths {

  public final static double EPSILON = 1E-6;

  /**
   * Linear Interpolation from first to second via alpha amount.
   *
   * @param first the first value
   * @param second the second value
   * @param alpha the alpha to calculate the double that is interpolated between first and second, normally a value between 0.0 and 1.0
   * @return the interpolated double via alpha
   */
  public static double lerp(double first, double second, double alpha) {
    return first * (1.0 - alpha) + second * alpha;
  }

  /**
   * Inverse linear interpolation from min to max to get the corresponding alpha from value.
   *
   * @param min the minimum value
   * @param max the maximum value
   * @param value the value to get the alpha from
   * @return the corresponding alpha
   */
  public static double invLerp(double min, double max, double value) {
    if (Math.abs(max - min) < EPSILON) {
      return max;
    } else {
      return (value - min) / (max - min);
    }
  }

  /**
   * Clamp a value between a minimum and a maximum.
   * @param value the value to be clamped
   * @param min the minimum value
   * @param max the maximum value
   * @return the value in Range[min;max]
   */
  public static int clamp(int value, int min, int max) {
    return Math.max(min, Math.min(max, value));
  }

  /**
   * Clamp a value between a minimum and a maximum.
   * @param value the value to be clamped
   * @param min the minimum value
   * @param max the maximum value
   * @return the value in Range[min;max]
   */
  public static float clamp(float value, float min, float max) {
    return Math.max(min, Math.min(max, value));
  }

  /**
   * Clamp a value between a minimum and a maximum.
   * @param value the value to be clamped
   * @param min the minimum value
   * @param max the maximum value
   * @return the value in Range[min;max]
   */
  public static double clamp(double value, double min, double max) {
    return Math.max(min, Math.min(max, value));
  }
}
