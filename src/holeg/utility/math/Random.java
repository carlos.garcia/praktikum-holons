package holeg.utility.math;

/**
 * A Helper class to abstract the implementation of the generation of a random number.
 */
public class Random {

  private static final java.util.Random random = new java.util.Random();

  /**
   * True or false
   *
   * @return the random boolean.
   */
  public static boolean nextBoolean() {
    return random.nextBoolean();
  }

  /**
   * Between 0.0(inclusive) and 1.0 (exclusive)
   *
   * @return the random double.
   */
  public static double nextDouble() {
    return random.nextDouble();
  }

  public static float nextFloatInRange(float min, float max) {
    return min + random.nextFloat() * Math.abs(max - min);
  }

  /**
   * Returns the next pseudorandom, Gaussian ("normally") distributed double value with mean and
   * standard deviation from this random number generator's sequence.
   *
   * @param mean      the mean of the Gaussian distribution
   * @param deviation the standard deviation of the Gaussian distribution
   * @return next gaussian value
   */
  public static double nextGaussian(double mean, double deviation) {
    return mean + random.nextGaussian() * deviation;
  }

  /**
   * Random Double in Range [min;max[ with UniformDistribution
   *
   * @param min minimum double value
   * @param max maximum double value
   * @return next random double value in range
   */
  public static double nextDoubleInRange(double min, double max) {
    return min + random.nextDouble() * Math.abs(max - min);
  }

  /**
   * Random Int in Range [min;max[ with UniformDistribution
   *
   * @param min minimum integer value
   * @param max maximum integer value
   * @return next random integer value in range
   */
  public static int nextIntegerInRange(int min, int max) {
    return min + random.nextInt(max - min);
  }
}
