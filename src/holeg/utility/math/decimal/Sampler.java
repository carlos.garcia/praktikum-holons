package holeg.utility.math.decimal;

import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * To sample floats and get basic statistical values.
 */

public class Sampler {

  /**
   * A map containing all samples.
   */
  public HashMap<String, SampleInfo> logMap = new HashMap<String, SampleInfo>();

  /**
   * Add a sample by a string key.
   * @param parameterName a key
   * @param sample a sample
   */
  public void addSample(String parameterName, float sample) {
    //Puts the sample in the right LogInfo container or create a new container if not exist jet
    if (logMap.containsKey(parameterName)) {
      SampleInfo info = logMap.get(parameterName);
      info.addSample(sample);
    } else {
      SampleInfo info = new SampleInfo();
      info.parameterName = parameterName;
      info.addSample(sample);
      logMap.put(parameterName, info);
    }
  }

  /**
   * Clears the sample map.
   */
  public void clear() {
    logMap.clear();
  }

  public String toString() {
    return logMap.values().stream().map(Object::toString).collect(Collectors.joining("\n"));
  }

  /**
   * A class to store all sample of a key.
   */
  public static class SampleInfo {
    public String parameterName;
    public int sampleSize = 0;
    public float min = Float.MAX_VALUE;
    public float max = Float.MIN_VALUE;
    private float sum = 0;

    /**
     * Returns the average of all floats.
     * @return the average
     */
    public float getAvg() {
      if (sampleSize > 0) {
        return sum / (float) sampleSize;
      }
      return 0;
    }

    /**
     * Stores a sample in the struct.
     * @param sample a sample
     */
    public void addSample(float sample) {
      if (sample < min) {
        min = sample;
      }
      if (sample > max) {
        max = sample;
      }
      sum += sample;
      sampleSize++;
    }

    public String toString() {
      return "[" + parameterName + " in Range(" + min + ", " + max + ") with a Average of "
          + getAvg() + " from " + sampleSize + " Samples]";
    }
  }
}
