package holeg.utility.math.decimal;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * A class with format shortcuts for common formats.
 */
public class Format {

  private static final DecimalFormat formatter;
  private static final DecimalFormat twoFormatter;

  static {
    // Initializes the formatters statically
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
    formatter = (DecimalFormat) nf;
    formatter.applyPattern("#.###########");
    formatter.setRoundingMode(RoundingMode.UP);

    twoFormatter = (DecimalFormat) nf;
    twoFormatter.applyPattern("#.##");
    twoFormatter.setRoundingMode(RoundingMode.UP);
  }

  /**
   * Format a double with variable amount of places.
   * @param places the amount of decimals after the '.' - separator
   * @param value the value to format
   * @return the formatted string
   */
  public static String doubleFixedPlaces(int places, double value) {
    return String.format(Locale.US, "%." + places + "f", value);
  }

  /**
   * Format a double value with two decimals after the '.' - separator.
   * @param value the value to format
   * @return the formatted string
   */
  public static String doubleTwoPlaces(double value) {
    return twoFormatter.format(value);
  }
  /**
   * Format a double value with ten decimals after the '.' - separator.
   * @param value the value to format
   * @return the formatted string
   */
  public static String doubleAllPlaces(double value) {
    return formatter.format(value);
  }
}
