package holeg.utility.listener;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * A functional interface to streamline lost focus events.
 */
@FunctionalInterface
public interface LostFocusListener extends FocusListener {

  void update(FocusEvent e);

  default void focusGained(FocusEvent e) {
  }

  default void focusLost(FocusEvent e) {
    update(e);
  }
}