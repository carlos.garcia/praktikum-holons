package holeg.utility.listener;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * A functional interface to streamline window closing events.
 */
@FunctionalInterface
public interface WindowClosingListener extends WindowListener {

  void update(WindowEvent e);

  @Override
  default void windowOpened(WindowEvent e) {
  }

  @Override
  default void windowClosing(WindowEvent e) {
    update(e);
  }

  @Override
  default void windowClosed(WindowEvent e) {
  }

  @Override
  default void windowIconified(WindowEvent e) {
  }

  @Override
  default void windowDeiconified(WindowEvent e) {
  }

  @Override
  default void windowActivated(WindowEvent e) {
  }

  @Override
  default void windowDeactivated(WindowEvent e) {
  }

}
