package holeg.utility.listener;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
/**
 * A functional interface to streamline resize events.
 */
@FunctionalInterface
public interface ResizeListener extends ComponentListener {

  void update(ComponentEvent e);

  default void componentResized(ComponentEvent e) {
    update(e);
  }

  default void componentMoved(ComponentEvent e) {
  }

  default void componentHidden(ComponentEvent e) {
  }

  default void componentShown(ComponentEvent e) {
  }
}
